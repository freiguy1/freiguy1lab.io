layout: post.liquid

title: "A hobby and an operating system"
published_date: 2013-06-11 11:42:05 -0500
---

Backpacking. Linux. Two things that one normally wouldn't read about in one blog post. And probably not even normally in two posts by one author. But get ready for some mind blowing content because I plan on talking about the great hobby of backpacking and Linux.

I recently bought a [backpack](http://www.amazon.com/TETON-Sports-Explorer-Internal-Backpack/dp/B001947FG8) for backpacking (because it's a new hobby I'm getting into). This backpack is from the company TETON Sports. Ok remember that. Next I like working in and normal use of Linux on a daily basis. If this blog doesn't make it clear enough, my favorite distribution is Arch Linux.

You'll never guess what Arch Linux and TETON Sports has in common! Their logos!!

![](http://3.bp.blogspot.com/-XfXiPXx2Mlk/UY-Y_WqIFNI/AAAAAAAABQU/9BmSiPu9TBE/s320/tetonsports.jpg)

![](http://danlynch.org/wp-content/uploads/2009/03/arch-linux-logo.png)

That is it.

Thank you.


