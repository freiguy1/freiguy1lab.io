layout: post.liquid

title: "Computer Desk"
published_date: 2017-11-30 13:43:12 -0500
# banner_photo: "https://lh3.googleusercontent.com/LRMzIybHt-SXFcTxQI1UkSCaMRKJFnwv5rCHbpAJwiVYuYQ6JCK0oOPNp4aZkD4pCBohv-1ZBsHx-iRPz4XiyBphlXzK_p096Xnu0Eh-lieVV9VZtQCQabSn2kJCzJQkLY4J_5RA8CUXF3XkdqCQUWWNYyhVrPaJlOHwFypaMIj-sKaaB3so1AF7LCfLNPQFwGZcdCaf5iK3Zn_xT2MgyXYmBfgQ39y8qEr6HbHNDhPSViQKNmF9uuIXUe6CIkbslL6Gm98n7pL_SalP9nxY6tsuA4eIzrga7Ty-Iqi4kC_hc1EPJjL-oxv541TUyeXyf96A5ObFJYAD-55G1nVdZNJcwBbMRKvSzassYuw0wq1l-75AqrZFyl4UU2vYvRf75c_JML_uaKAb8teag18JCtfFAPSV6OktA2C7TNQf2DABbcFXyQRWd7koRD9ZDnzLs9TDR6PNbs3n1b69f2ebW24UgdwPCZm2DAEfk72YCY_rZabtn-4YmjRV3OJ1VGRpzyyRR4dxiOVwN7Zbvj_70yU26e-2OKlXuaEA4AAN3oq4xu3DBT0GM2Ovmc6AsCHklTbIUaQobIHe3QQBylgW4mfTYcy1xRiYuGo0B4gEpi0=w1141-h760-no"

---

I just got an exciting new job where I'll be working from home 4 days a week. For the last 6 years, my home desk has been comprised of a half-size folding card table. It was pretty much the cheapest thing my recently-graduated self could find which could support a couple monitors and a keyboard. Now with a full time job working from home, I wanted to step it up in the home office department.

## Electric sit/stand base

I ordered the DIY electric desk mechanism from [Autonomous](https://www.autonomous.ai/).  It was under $300 for the base, which is very affordable compared to others. It has 4 customizable presets, and has a display which shows the height in inches.

## The top

I made the top with some red oak I had from my great uncle. I wanted it to be 28" deep and 6 feet long.  This size was not only very adequate for a desk with 3 monitors, but fit perfectly in a corner which I had designated as my office.  It took 4 boards to join edge to edge to make basically one big board the size of the desk. I used biscuit joints to keep them aligned. Those things have been life savers for large panel glue-ups like this.

After the boards were glued up, I cut it to length with a circular saw guided by a straight edge. I then rounded the corners and cut out a small concave section for my torso to fill. I then used a roundover bit in the router to go all the way around the top of the beast. After all that was done, it was on to sanding. I used 80, then 110, then 220 grits, until it was very nice and smooth.

I normally don't use stain, but I wanted to try something different with this one. My wife  and I agreed that espresso seemed like a unique and good-looking flabor. I stained it and was very impressed by the results! After that was dry, I used 3 coats of a water-based poly as a top coat.  It all went on well and I was really pleased with the results!

I made 4 braces on the bottom of the desktop to attempt to keep the boards straight. They run perpindicular to the grain, and are attached in a way to account for the top moving throughout the seasons.

Thanks for checking out this project!

![](/img/2017/desk1.jpg)

![](/img/2017/desk2.jpg)

![](/img/2017/desk3.jpg)

![](/img/2017/desk4.jpg)

