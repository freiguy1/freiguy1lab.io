layout: post.liquid

title: "Dog House Heating System - Part 2"
published_date: 2013-10-18 15:47:38 -0500
---

In case you're interested, here's [Part 1](/posts/dog-house-heating-system-part-1.html).  Project Completed! Since this wasn't the most complex project, it looks like I'm only going to have 2 parts. I was able to (with some assistance) solder all the components to the board, and the first time testing revealed that everything had been soldered correctly! Normally this doesn't happen and I'm left having to clean up some mistakes which isn't very easy when soldering is involved.

I was able to successfully solder the temperature sensor upside down. With the tmp36 (sensor) sticking 5/8" lower than the rest of the board, I can stick it through an opening in a wall/ceiling so that it's measuring the actual temperature inside the dog house.

Check out some pics and video I made as we were finishing it up:

![Controller board attached to top of ceiling](https://lh3.googleusercontent.com/-sT15GWHpxX8/UmGZBvMJQ4I/AAAAAAAAHJo/HKQN9lk01zU/w427-h569-no/20131017_204523.jpg)

![Wiring some more](https://lh4.googleusercontent.com/-h8VZfdVt0lU/UmGZEiAYR9I/AAAAAAAAHJ8/CwrlMg-0OAo/w427-h569-no/20131017_204618.jpg)

![Furnace-looking dog house](https://lh4.googleusercontent.com/-InmekXABPcQ/UmGZE2rjHlI/AAAAAAAAHJ4/LZIQW_aSbKs/w427-h569-no/20131017_213034.jpg)

<div style="text-align:center;"><iframe allowfullscreen="" frameborder="0" height="356" src="http://www.youtube.com/embed/BdfkXMlGl20?feature=oembed" width="474"></iframe>

</div>
