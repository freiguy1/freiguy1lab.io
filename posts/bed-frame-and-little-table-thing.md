layout: post.liquid

title: "Bed Frame and Little Table Thing"
published_date: 2012-05-24 00:04:20 -0500
---

This year I was lucky enough to get a tax refund and I've been wanting some tools for a while so I decided to splurge on some nice cordless power tools. I got a set of Milwaukee Lithium 18V tools that include a hammer drill, circular saw, sawzall, and flash light. You can buy additional tools/batteries for this set and they're supposed to last quite a while.

After getting the tools, I had to get my hands dirty and build something. So I got out my pencil and notebook and came up with 2 designs: a bed frame for my double-high air mattress, and a small table that rolls around and I can use for whatever I want. The need for the latter came from not having enough room around the grill to hold the utensils and food.

**Bed Frame**

Right now my bed-situation is sort of unique. I'm renting a house for an indefinite time and didn't want to buy a full-fledged bed frame, box spring and mattress. What's a man to do instead? Air mattress is the only other obvious solution (a bit of not-so-obvious sarcasm intended there). So I bought one and slept on it for quite a while, but I had some problems. Namely, the more I slept on the air mattress, the more it seemed to be adopting the shape of an oval (from a top-down view) as opposed to a nice rectangle. The unintended consequence of this was that my sheets kept coming untucked. Another consequence was that I couldn't have a side table neatly butted up against my bed. Another problem was that by its nature, it was unsteady. Getting off of it (especially when waking up earlier than my body wanted) could be difficult and there was much movement activity whenever I wanted to turn over.

How to solve all these problems? Create a bed frame for my double high twin air mattress! So that I did. It's made from like 9 2x4 studs and some dowels which is quite cheap in total. The four corner posts have 2"x4" holes (mortises) through them so the side posts can slide through the middle. Then the side posts are held in by a dowel going through the inside and outside of its intersection with the corner post. Why? Well now all I need is some strong hands or a hammer to disassemble! The ends are screwed into the corner posts so they don't come apart easily. But this can be disassembled in like 2 minutes and fit in my SUV very easily! It also fixed all my problems.

**Small Table Thing**

This project requires a little less explanation. The wings on my grill (as seen in the picture) aren't big enough to hold larger plates/trays of food and all my cooking utensils. I ran out of room quickly whenever trying to cook an appropriately sized (larger) meal. So I decided to build me-self a little table. It's as tall as the grilling surface on my grill and like 4' wide or something. The actual dimensions are written down in a notebook that's about 6 feet away from me as I'm laying in my nicely framed air mattress bed. I added a bottom shelf area so anything you desire can be put there. Eventually I'll be staining/finishing it so I stop poisoning everyone I have over for a BBQ, but the wood needs to dry some more.
