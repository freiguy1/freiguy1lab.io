layout: post.liquid

title: "C-Shell"
published_date: 2012-05-17 22:56:39 -0500
---


## Introduction

A shell is a program that mediates between the user and the kernel. You can use your 'shell' on Windows by going start > run > type cmd > OK. As a fun fact, my website here <del>is</del> was programmed completely using one of these shells; no graphical development environment was used. This was more to challenge myself then it was for enjoyment, but after using a shell so much I've started to see its advantages. For one of our projects in CS362, we were required to implement our own shell. Check it out.


## Project Information

This project is an interesting one. For my Operating Systems class at UW - Eau Claire, our first assignment was to implement a [UNIX shell](http://en.wikipedia.org/wiki/Shell_(computing)) of sorts. We were required to implement the following operations with the shell:

- run a basic command
- run a command with arguments
- run a command whose output is redirected to a file
- run a command whose output is appended to a file
- run a command whose input is redirected from a file
- run a command whose output is piped to the input of another command
- run a command with multiple pipes like those mentioned above
- run a command which gets executed in the background (top &)
- mix and match all of these requirements!

My partner on this project was my good friend <del>and roommate</del>, Luke Komiskey. We put quite a few hours working on this, and I think the finished product works pretty well. From what I've tried, the program runs all of the above mentioned requirements. Some things that it could be extended to do: change directory (I know, but it wasn't a requirement and time is limited), implement the jobs command to list current backgrounded jobs, implement the fg and bg commands that let you manipulate what processes are in the foreground and background, account for more memory leaks(!), and listen for ctrl+C and ctrl+Z signals and handle them correctly.


