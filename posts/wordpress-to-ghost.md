layout: post.liquid

title: "Wordpress to Ghost"
published_date: 2015-07-28 22:13:18 -0500
---

*note: Out of date. I'm now using cobalt, and that transition is outlined [here](/posts/ghost_to_cobalt.html)*

Since I'm moving to a new house without fast internet, I've decided to move my operations to the cloud!  During this transition and after considering the work involved, I decided moving this blog from Wordpress to a more preferred blogging system would be worth it.  I've already hosted a different Ghost site, and have helped my wife host [her's](http://kayteelynne.com).  I've space on an Ubuntu server running on Azure funded by credits provided by my job (excuse all the participle phrases), so once again, hosting is free other than paying for the domain name.

The transition of content was only slightly painful.  Ghost developers have made a plugin which exports all the Wordpress content into a json.  It can then be imported on a Ghost blog.  I had a few problems during this process.  The exportation worked well, however, there were hundreds of users (bots/hackers?) registered, and some didn't have valid emails.  Ghost didn't appreciate that, so I manually removed all users but myself in the json file.  The next issue was after the content got imported to this new blog.  There were several unrecognized characters which were all displayed as diamond question marks.  I think it was some problem with encoding. Some spaces would appear this way, all apostrophes, all quotes, and all colons.  I manually went through each imported post and corrected them, which was a bit tedious.

![](https://lh3.googleusercontent.com/m1t1lmCmtDlxhoqISreRFaL0mJl0rg99gp6E8CMud0g=w334-h593-no)

After these issues were remedied, I think we're set.  The next piece to work on will be finding a theme so I can replace the default, which is currently enabled.
