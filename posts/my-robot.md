layout: post.liquid

title: "My Robot"
published_date: 2014-12-10 13:01:07 -0500
---

I've been building a robot in my free time for some fun.  Recently someone came to me and asked if I'd write up a parts list so others could create a similar robot of their own.  Well this is a great idea.  I should probably also open source/publicize the code, which has been written in python as of now.  That will be step number two.  So lets get onto the juicy parts:

##### Chassis: Rover 5. $60. ([Pololu](http://www.pololu.com/product/1551)) ([SparkFun](https://www.sparkfun.com/products/10336))

This is the most expensive part.  There are a number of chassis which are less expensive if your budget is an issue.  I chose this one because I like the tank treads, the ability to adjust the height, and the fact that it comes with built in encoders, if you get that model.  I did have some issue w/ the treads wanting to come off of the wheels, but adjusting the angle of the wheel arms has seemed to fix that.

##### Motor Driver: TB6612FNG. $5-9. ([SparkFun](https://www.sparkfun.com/products/9457)) ([Pololu](http://www.pololu.com/product/713))

This is the piece which allows your brain (microcontroller/microcomputer) to control the DC motors in the chassis.  The motors require more current than the I/O of the uC can provide, so that's why this component is needed.

##### Battery: 7.4V Li-Poly. $14. ([SparkFun](https://www.sparkfun.com/products/11856))

Normally, you'd probably use 4-6 AAs for this build, but my microcomputer which I'm using for this build takes a bit more power.  If you use a microcontroller which doesn�t use much power, you wouldn't need to use such an intense battery.  This battery is Lithium Polymer which means it requires a [special charger](https://www.sparkfun.com/products/10473).  Keep this in mind if you go Li-Poly.

##### Voltage regulator: L7805. $1. ([SparkFun](https://www.sparkfun.com/products/107))

In order to power the brain, we need a way to limit the battery's voltage to that which it can use.  Mine needs 5V, so this chip works decently.  I may eventually need to upgrade to allow for more current, because I think I'm at the current peak for this little guy.

##### Brain: Beaglebone Black. $45-60. ([Adafruit](http://www.adafruit.com/product/1278)) ([Amazon](http://www.amazon.com/Beagleboard-BBONE-BLACK-4G-BeagleBone-Rev-C/dp/B00K7EEX2U/ref=sr_1_1?ie=UTF8&qid=1418235986&sr=8-1&keywords=beaglebone+black))

Now just like all the other pieces of this build, you certainly have the potential to be creative in this category.  The Beaglebone black is actually a whole computer on board that runs Linux.  Mine is running Debian.  Here are some other options that you might consider:

- Microcomputer route - [Raspberry Pi](http://www.raspberrypi.org/)
- [Hummingboard](http://www.solid-run.com/wiki/index.php?title=HummingBoard_Hardware)
- [Banana Pi](http://www.bananapi.org/)
- Microcontroller route - [Arduino Uno](http://arduino.cc/en/Main/arduinoBoardUno)
- [Other Arduinos](http://arduino.cc/en/Products.Compare)
- Arduino Uno Alternatives ([Redboard ](https://www.sparkfun.com/products/12757)for instance)
- Native USB Arduino-like ([SparkFun](https://www.sparkfun.com/products/11117), [Pololu](http://www.pololu.com/product/3103))
- [Netduino](http://www.netduino.com/)
- [Parallax Propeller](http://www.parallax.com/product/32910)
- [mBed](https://mbed.org/)
- Probably hundreds maybe thousands more.
- Bare Chips (you must purchase programmer separately) - [Atmel tinyAVR chips](http://www.atmel.com/products/microcontrollers/avr/tinyavr.aspx)
- [Atmel megaAVR chips](http://www.atmel.com/products/microcontrollers/avr/megaavr.aspx)
- [PIC chips](http://www.microchip.com/pagehandler/en-us/family/8bit/) and others which I don't have experience with

##### Bluetooth: BlueSMiRF. $25. ([SparkFun](https://www.sparkfun.com/products/12576))

This will allow me to control the robot remotely.  My plan is to control it via Android smart phone one day.

Some other other things I've bought revolve around mounting these items to the actual robot.  For instance: a [400 pin breadboard](http://www.pololu.com/product/351), a [mounting plate](http://www.pololu.com/product/1533), other random mounting hardware, wires and some spare resistors and capacitors.


