---
published_date: "2022-04-12 22:56:49 +0000"
is_draft: false
title: 2022 Gardening Journal
layout: post.liquid
---

Another year, another garden! In this post I plan on keeping an ongoing journal about my gardening experiences for the year. In some areas I'll probably be less verbose than 2021, and maybe in others, I'll be more descriptive.

### Helpful Links
- <a target="_blank" href="/img/2022/seed-starting-calendar.pdf">Seed starting calendar for my area</a>

### Tips for Next Time
- Instead of starting plants at random times based on last year's notes and friends/families, sow them according to the calendar linked above!
- Any old seeds (> 1 year old), use 2 or 3 seeds per cell. They usually don't germinate as well when they're older so what have you got to lose? Just pull out/replant extras if needed.
- Most plants don't need sun to germinate! Throw trays above refigerator for a slightly warmer temperature until 1/2 of them are sprouted.
- 2021 last frost was May 28. While that was pretty out of the ordinary, might as well plant around/after that date so long as I have room for starting plants.
- Generally after plants have reached a half inch to 1.5 inch, they will have to be transplanted into a bigger pot than those starting cells. Keep an eye out for when they stop growing.
- Mixing a little chicken poop with potting soil when repotting is helpful! On second thought (a few weeks later), the potting soil might have enough? Some tomato plants maybe showed signs of having too much food? Like turning a very dark gray/green and leaves curling in. This could've been from something else too. Might be worth experimenting.
- If I start tomatoes as early as this year, and don't want to plant until end of May, transferring them into 4" square plastic pots would be better than the little cardboard ones I used this year. They seem to stop growing after hitting 4-5 inches.
- Greenhouse shouldn't be getting into the 90s if possible. Leaves start to curl and get burned at the tips.
- When planting seeds in the garden, be mindful of what you cover them with! Hard chunks of compost, even firmly packed clay-soil do not allow seedlings up through. Given the garden state, cover with seed-starting soil instead, and they (at least the peas) seem to come up easier.

For my birthday, my mom got me a gardening book that I'm going to take some advice from. The book is called "Mini Farming, Self-Sufficiency on 1/4 Acre." The purpose and motivation of the book is to essentially min-max your garden output in terms of produce and money saved year-to-year on a minimal amount of land. I'm not that hardcore, but it contains a lot of good tips I'm going to start using! First, I'm not going to plant in rows. Rather, I'm going to arrage plants in grids which is common in the [intensive agriculture](https://en.wikipedia.org/wiki/French_intensive_gardening) technique. Here's my new garden layout. Notice the large sections, rather than rows.

![garden map](/img/2022/garden-map.jpg)

The slashed areas will have no plants. They'll be walking paths. This will allow easy access to the middle of each patch from the sides. I also plan on composting a bit more effectively. I composted last year a bit and hope to keep it up!

# Seed Starting Greenhouse!

So this woodworking and electronics project probably deserves its own post, but I'll give a high level overview. My dad had some large insulated windows (70" x 28.5") from his previous job at a glass factory. He gave me one of those, and I used it as the roof for a mini-greenhouse since starting seeds in our house is not very easy.

In addition to the enclosure and glass frame, I built and programmed the electronics part. This includes a temperature sensor, heater (controlled by relay), and venting system for cooling (controlled by servo). As I'm writing this, the greenhouse is not totally complete yet (specifically the venting system), but it has been keeping plants alive for over a week! On the sunny days I need to prop open the top otherwise temperatures can reach over 150 F even when the outdoor temperature is under 50 F.

![greenhouse 1](/img/2022/greenhouse-1.jpg)

![greenhouse 2](/img/2022/greenhouse-2.jpg)

The electronics also logs data to the internet where it's stored and graphed. Check out the upper-right graph. Notice the zig-zag lines when the temperature gets down to ~63 F. That shows my circuit & code turning the heater on when the temp gets low and off when it is heated enough. That happens at night time, then it heats up naturally during the day.

![graph screenshot](/img/2022/greenhouse-graphs.png)

Enough of that, like I said it deserves its own blog post!

# Starting Seeds

As of today (4/12) I've started three sets of seeds separated by two weeks. The list of plants is pretty large, so I won't list them all here. Some are doing better than others. Peppers are slow, tomatoes are doing decently. Salvia flowers seem to be dying after they get 1/4" tall. Zero of six red onions have germinated.

# Repotting 4/25

Is repotting a word? My text editor says no. Well anyway, many of the tomatoes and other plants didn't like their little cells. So I've moved them into small biodegradable cups which can just be put straight in the garden when I transplant. So far, I've moved 13 tomatoes, 3 peppers, and 12 moss roses (four to a container). It's late April and the tomatoes are looking pretty good; one especially stands out. I look forward to what they'll look like when they're ready to go in the garden in late May or early June!

![repotting](/img/2022/repotting.jpg)

# Repotting part two 4/28

I noticed several plants weren't growing any longer in the starter cells which I most recently planted (April 3rd). I decided to transplant the rest of the seedlings to larger containers. It turned out to be more of a struggle than anticipated due to the children I was simultaneously watching finding dog poop in the yard, but I got through it. Broccoli, sesame, cabbage, lettuce, spicy peppers and cockscomb flowers repotted! In this photo, they're most of the plants on the left side.

![repotting](/img/2022/repotting2.jpg)

# Fertilizing, compost, tilling 5/3

This year has been unusually and depressingly cold and wet. It's taking forever to get to the garden and work with it since it's staying frozen or muddy. Today we had bits of sunshine and the temperature broke 50F, so I decided to see how the rototiller worked. The attempt was successful, so I devoted the night to getting the garden bed ready for planting!

First I sprinkled 10-15lb of dried chicken poop fertilizer on the top. I don't have a scale, so it's hard to be precise. However I knew the garden needed it so I wasn't bashful. The instructions on the fertilizer bag puts our garden size at about 15lb before planting and 15lb after plants are well established.

Next I gathered one fourwheeler-trailer-full of homemade compost. This is mainly composed of old decomposed mulch, grass clippings, and leaves from the fall. This pile has not been added to in over a year, and I used my tractor to flip it throughout last summer. I think it was looking very nice. One trailer-full allowed me to throw a shovel load over the whole garden.

![trailer compost](/img/2022/trailer-compost.jpg)

Lastly I tilled then raked. Oliver (5) even gave the rototiller a shot and did pretty well! Everything is still pretty mucky and wet, but we're borrowing the tiller from my grandma-in-law, so I want to return it as son as possible.

![tilling](/img/2022/tilling.jpg)

![oliver tilling](/img/2022/oliver-tilling.jpg)

![after raking](/img/2022/after-raking.jpg)

# Planting corn and greenhouse update 5/13

Today my niece was born! Thea Jude Olp!

I planted some peaches & cream sweet corn today. The seeds come from Hoss Tools, and I have high hopes! Maybe 40-50 seeds spaced about 7" apart in rows about 10" apart. I have a bunch of peas planted, but they haven't come up yet. I bought those seeds from the co-op in town. A week ago or so I also planted onions and potatoes. The onions aren't seeds, but bulbs and they're looking very good. I think my tomatoes are really wanting to get planted outside. They're outgrowing their cups.

The weather has been unseasonably hot after an unseasonably cold April and early May. Today's high was 91. Tomorrow we get a break with like a high of 83.

Here are some updates from the greenhouse for today for future reference.

![may-update-1](/img/2022/may-update-1.jpg)

![may-update-2](/img/2022/may-update-2.jpg)

![may-update-3](/img/2022/may-update-3.jpg)

# Transplanting tomatoes 5/15

The tomatoes were starting to look a little rough in the greenhouse. I'm not exactly sure why but here are some possible factors: not enough air circulation in the greenhouse, too much concentrated sun in the greenhouse, outgrew their containers, or the chicken poop I added to the potting soil created an environment with too much concentrated fertilizer. So I decided to move them out to the garden. After all, last year was a fluke (the late-May freezing temps), and I'd rather have them die from cold (where I have a chance to cover them) than dying in my greenhouse!

After transferring eight plants, I took some sawdust from woodworking and spread it around the base of every plant. This might help the clay-heavy dirt from becoming cement-like and cracking like the desert.

![garden-kids-1](/img/2022/garden-kids-1.jpg)

# Hailstorm! 5/18

Well just like last year, mother nature seems to have it out for me! Three days after transplanting the tomatoes there was a very significant precipitation event: a hailstorm! And a bad one, at that. We received marble-sized hail that accumulated to about an inch deep. The tomatoes look rough. One tomato is totally smashed so that certainly won't make it. Immediately the next morning I ran to Menards and grabbed a pack of 6 more "big boy" tomatoes. The broccoli and cabbages look pretty rough too. After sending pictures to my dad, he thought they'd be okay mostly. At least now I have some backups.

It was pretty disappointing to watch the hail come down. I had done a lot of work preparing for the garden this year including building a whole custom climate controlled greenhouse and starting all those damaged plants from seeds 3 months ago! I guess the moral of the story (which is consistent with the late frost from last year) wait until June for transplanting!

![hail-tomato](/img/2022/hail-tomato.jpg)

![hail-tomato-2](/img/2022/hail-tomato-2.jpg)

![hail](/img/2022/hail.jpg)
I took this picture in the morning after it had rained a lot throughout the night and a lot of the hail had melted. This must've been a spot where the a lot of ice had drained.

# Bean and more corn planting 5/27

The header says it all! I planted the second half of the sweet corn. Then I planted green beans! This year I planted bush beans called "Provider Bush Beans" from Hoss Tools. For this area of the garden, I brought in a bunch more compost from my pile. I planted the beans in a 6" grid pattern. This ended up needing 84 seeds! We'll see how that turns out.

The greenhouse is going well! I installed a small 5V DC exhaust fan to compliment the servo-powered vent. Disappointingly it's still not enough cooling power to keep the greenhouse below 90F. So we've just kept it propped open lately. At this point, I've got a couple tomatoes which are a foot tall and a foot wide!

![greenhouse 3](/img/2022/greenhouse-3.jpg)

One fun thing been happening this spring is a robin's nest that we've been able to watch throughout the last month! It's nice and low in one of our pine trees.

![robin nest 1](/img/2022/robin-nest-1.jpg)

![robin nest 2](/img/2022/robin-nest-2.jpg)

# Returned from vacation 6/20

Wow, it's been a long time since journaling but it doesn't feel like that long! My family just returned from our first family vacation to Mackinac Island. We were gone 8 days and it is really remarkable to see the change over that timespan. The tallest corn plants are maybe 20-24". The potatoes were the most impressive as they're about the same height. All the tomatoes look great except for one which seems pretty stalled.

Since planting beans and corn, it's been mostly maintenance. I've installed some trellises for peas, done a lot of weeding and watering. I even fertilized with more chicken poop on top around all the plants on 6/19. We've had some flowers blossom (moss roses, and you can start to see the flower of the cockcomb). 

In our standing garden I've been able to harvest some lettuce! Kaytee and I whipped up some wraps using the lettuce yesterday and we both really liked it - Kaytee especially since it reminded her of spinach.

I have some family/garden pictures from before vacation and after. Enjoy!

### Before vacation

![garden kids 2](/img/2022/garden-kids-2.jpg)

![pre vacation beans](/img/2022/prevaca-beans.jpg)

![pre vacation corn](/img/2022/prevaca-corn.jpg)

![pre vacation tomatoes](/img/2022/prevaca-tomatoes.jpg)

### During vacation

![during vacation](/img/2022/during-vacation.jpg)

### After vacation

![garden kids 3](/img/2022/garden-kids-3.jpg)

![post vacation](/img/2022/post-vaca.jpg)

![post vacation beans and corn](/img/2022/postvaca-beans-corn.jpg)

![post vacation tomatoes](/img/2022/postvaca-tomatoes.jpg)

![post vacation middle](/img/2022/postvaca-middle.jpg)

# Harvesting & preserving 8/4

The garden has been doing great! Some notable things are that our pea plants have reached 7' tall, we're getting a ton of beans, peas, cabbage, broccoli, and cucumbers (even though we only had 2 plants, we've harvested 12 nice cucumbers so far). Corn looks great, is the tallest plant in the garden, and has many ears that are getting pretty big! Cherry tomatoes are just starting to barely turn orange and red; I think I've picked two so far. The larger tomatoes are all still very green and hard, but growing well.

We've also been able to start on our preserving campaign for this year! We have big plans to do a bunch of preserving, so I hope we follow through. So far we've done one round of green bean canning. This involved three rounds of picking, every other day, then a saturday afternoon of canning. In that first round we canned 16 pints! Our little 10'x4' bean section has been doing so well. It's amazing to see how much that area can produce.

![early aug cucumbers](/img/2022/early-aug-cucumbers.jpg)

![early aug beans](/img/2022/early-aug-beans.jpg)

![early aug corn](/img/2022/early-aug-corn.jpg)

# Pigging out 8/29

Well the garden has been amazing this year. We've gotten to the end of the beans, peas, and dug all the potatoes. Sweet corn, and cherry tomatoes are in full swing. Large tomatoes are beginning to ripen. I'll shower ya with harvest pics. I also made sauerkraut about a month ago using a couple cabbages from the garden. I've taste tested one jar and kind of liked it! Four weeks seemed like a good amount.

![cabbage](/img/2022/cabbage.jpg)

The tomatoes are officially taller than me!
![tall tomaotes](/img/2022/tall-tomatoes.jpg)

I harvested around 60 potatoes from our 12 plants! This picture doesn't give it justice, but there were lots!
![potato harvest](/img/2022/potato-harvest.jpg)

![garden kids 4](/img/2022/garden-kids-4.jpg)

![late-aug-corn](/img/2022/late-aug-corn.jpg)
