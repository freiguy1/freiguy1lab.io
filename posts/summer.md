layout: post.liquid

title: "Summer!"
published_date: 2012-06-29 10:03:10 -0500
---

It's been a great summer so far. I've been learning a ton at work and at home. At work we've managed to switch the system we're using as the front end of our huge project 2 times meaning we've started 3 times! The latest switch has been in the direction of native mobile development, which is exciting. �At home I've been straying away from the physical computing side and experimenting with java RESTful services. That's been an interesting journey. I've ended up using Eclipse as an IDE, Apache Tomcat 6 server for hosting, Jersey for the Java RESTful implementation, and the Jackson JSON converter for input/output. With all these technologies together, I've started to build a pretty decent service oriented web API.

Other than techie stuff, my summer's been really busy. Camping, hiking, softball and kubb tournaments, volleyball, camp fires, fishing and weddings have been doing a very good job at filling in my schedule. Sometimes it's good to be busy, but other times I wish time would slow down a bit!

Sorry about the lack of updates that are happening to the website! Life is keeping me quite busy. I usually get my geeking on more so in the wintertime, so look for it then!


