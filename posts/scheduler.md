layout: post.liquid

title: "Scheduler"
published_date: 2012-05-17 10:01:39 -0500
---

#### an easy way to see your busy week


## Intro

This program I made whilst bored on my own once back in 2008. It basically helps you see what your week looks like in picture form. Classes (or work) gets plotted out on Monday-Friday depending on what input is provided. This program has been well worth the effort, as many people have used it over the years to help roommates and friends know where each other is throughout the week.


## Background

I made this program during a week back in 2008 I think mostly out of boredom. I had been getting slightly frustrated about how long it took in excel, paint or even calendar programs to just do this simple operation say I wanted to post it up outside my door for roommates, attach it in an email, or post it to Facebook. So I decided to sit down and make a program.

## Program Info

My scheduler simply takes in a set of inputs classes and outputs a picture of your week. The inputs for each class is name, start time, end time, and the days the class is on. Times need to be in military time and without punctuation. Once all of this is completed correctly, you'd just hit the Go! button on the bottom and it will show you your week. One neat thing about this program is that the picture is totally resize-able. You can have it be a small picture, or blow it up to full screen. Everything gets painted dynamically depending on the size of the window. Try it out!


## Try it

This program comes packaged in an executable .jar file. This can be run using the Java Runtime Environment. Most computers now-a-day have it, but if not, you can get it free: [here](http://www.java.com/en/download/manual.jsp). Once you have the JRE installed you simply need to double-click on the .jar file and the inputs should be there waiting for you. Let me know if something isn't working as planned, and maybe I'll dig back into the code to help you out. Also, there is no (or very little) error checking, so keep that in mind when filling in the information!  


