layout: post.liquid

title: "Sprint to Straight Talk"
published_date: 2013-06-20 09:54:02 -0500
---

As a tech dude, I like having access to the wide world of information from right in my pocket. I've reinforced this fact about myself over the last month during which I've been using an old [Sanyo SCP-3100](https://www.google.com/search?q=scp-3100&oq=scp-3100&aqs=chrome.0.57j0l3.1882j0&sourceid=chrome&ie=UTF-8). My last smart phone, the HTC Evo 3D died about a month before my 2 year contract with Sprint was up. I've been using Sprint's unlimited everything plan for the last two years, and it's been going fine except that it's slightly more than I'd prefer to pay. With a 20% discount from my employer, I was still paying ~$73/mo for this plan. After doing some research, I found [Straight Talk](straighttalk.com) which offers an unlimited talk/text/data plan for $45/mo. So this blog post expands on the details involving switching between these two providers.

I started by doing some research, like most mid-twenties money-conscious people. Straight Talk seemed to be the provider that fit me the best, because I wanted unlimited everything and a good network. The next step with Straight Talk is to figure out which phones work in your area (because they've many phones which operate on different networks: Verizon, AT&T, Sprint and maybe more). The only phones available in my area were those on the Sprint network. I ended up choosing the [Motorola Defy XT](http://www.motorola.com/us/consumers/MOTOROLA-DEFY-XT/m-DEFYXT,en_US,pd.html). This is one of Motorola's older but more rugged Android phones.  I chose it because it was relatively cheap ($150), had everything I needed (bluetooth, two cameras, and Android OS), and had decent reviews, at least from the customers who knew what they were talking about.

After having chosen a phone, I needed to wait until my contract ran out. When this happens, Sprint will keep a plan going for you month by month. During this month to month subscription time is when you want to make the switch to a different carrier. In order to make the switch from Sprint, I needed two things for Straight Talk account number (9 digit number for me) and PIN (6 digit number for me). After having acquired the new phone and those two numbers, I went to Straight Talk's website and started the transfer process. It cost $45 for the first month/to get the transfer started unless you've already bought a one month unlimited card or a different one from Walmart.

I started the transfer process at about 9:00 pm and it completed at about 7:30 am the next morning. This wasn't the worst wait, and it was mostly while I was sleeping. After my old phone could no longer make a call, I simply restarted my new phone and it set itself up. After syncing my Google acct, all my contacts were synced and now I'm ready to go. I already had someone lined up to buy my old Sanyo, so that went quite smoothly. The next step, if there is one, would be to install Cyanogenmod so I can use the latest version of Android. Right now I'm stuck w/ 2.3, which seems strange coming from a newer version.

I hope this post helps someone out there; I was slightly lost when doing the switch, so I thought it'd be good to document it!


