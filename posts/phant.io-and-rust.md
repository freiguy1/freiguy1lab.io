layout: post.liquid

title: "Phant.io and Rust"
published_date: 2014-07-30 15:49:07 -0500
---
Lately I've been exploring the language of [rust](http://rust-lang.org).  I've really been enjoying getting back to the lower level style of a systems programming language. Rust has a lot of unique advantages over its competitors like C/C++, Go, D, and others as you can read about in other blog posts after a couple Google searches.  After doing the first ~30 [Project Euler](http://projecteuler.net/) problems in rust ([github repo](https://github.com/freiguy1/rust-project-euler)), I started to get a decent handle on the language in its current state (the language is constantly evolving).  

*Experimenting with rust syntax highlighting:*

```language-rust
// This is the main function
fn main() {
    // The statements here will be executed when the compiled binary is called

    // Print text to the console
    println!("Hello World!");
}
```

So naturally I decided to start a new chapter in my adventure learning the language.  My goal was to make a usable piece of software that performs a certain task well!  So I decided to create a library for interacting with Phant.io.

That leads right into a quick intro about [Phant.io](http://phant.io).  This is a web service application created by the wonderful, ingenious, and creative people @ [SparkFun](http://sparkfun.com).  They describe it nicely as

> a modular node.js based data logging tool for collecting data from the Internet of Things. It is the open source software that powers data.sparkfun.com, and is actively maintained by SparkFun Electronics.

Check it out in action at [data.sparkfun.com](http://data.sparkfun.com).  They've made it extremely simple to push, clear, and get data.  On their [libraries](http://phant.io/libraries/) page it shows a number of projects the general public has created for interacting with Phant.  It was nice that I'd learned about rust and Phant at the same time because there was opportunity to create a Rust library for Phant!

So that is exactly what I did.  The code is [here on github](https://github.com/freiguy1/phant-rust).  To me, it's pretty easy to use and [Cargo](http://crates.io) makes it easy to add to your project (simply add 2 lines to your project's Cargo.toml).  Now you can push data to your phant repository with just a few lines straight from your Rust project.

Using this library, I've been able to keep track of my dynamically changing IP addresses at [my very own data.sparkfun.com stream instance](https://data.sparkfun.com/streams/7JJpyOqxDrfbx6NEOObX) as a real world example.  My servers are using a cron job to upload the data every 15 minutes.


