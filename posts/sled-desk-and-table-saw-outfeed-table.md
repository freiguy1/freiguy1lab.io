layout: post.liquid

title: "Sled, desk and table saw outfeed table"
published_date: 2015-09-21 04:50:07 -0500
---
### Table saw built!

This weekend I spent many hours in the shop.  It was my first weekend with my new table saw.  It got delivered this last week on Wednesday.  After work on Wednesday, I was able to completely put it together with a little help from my wife. It was probably a 4-5 hour process, but simply because there were many steps.  I didn't get held up at all; SawStop's directions were amazing, which is what I expected since everyone says that. Here's the completed saw!

![](https://lh3.googleusercontent.com/wolbQt2d4r5SQpu9rM2J9VlUVCAhW-an1yGdGCJVjPj64wlCtLqFyfVbAfwct9sLN3wX3_gmwxHRIaxfrkUAXehG3gpugmsXBKAZ9yXJ9Bls_Hc3hY06vOrQjRbQKX7cXPOJ4pGfMp1731fxG3YrvBT0EiojEuPZxI2i7Wibf7kDFvEWV6el3oDUl_k0GLn2sTZg3swKjCZyzMCkAFT-wN-09NR4e4QRlJTFu4qppCA9ttx4QMNxmcgMqczXb-1N6nT2zo8o3MBo2jHSaCbhgebAxJPUkqhg7a0ETYfTIwtL_yhqEMakqoD2_boKFf7-wN51yHqTwwC5q8fjBpgTadeS6fwkcm1B-w-bHPb4M7gcMmEs2CFsDb7mGCDCQlAn_zuXydk5j2pZONQW03P24Q0F9tNJr_A4_gc4Pb5NBTQg__exolyyl4RkrpYhPn84108pnaM6UoMcO2fKMxNHzlH7wzJLodzM5ND5Lfh_8rKZ692-y4OmvvjSKBCIyOU9Ek2PlZTndwKmyY-uE_d_cB2FReuq8QegJua37ralsfc=w2979-h1676-no)

---

### Table saw outfeed table

Soon after completion, I thought the saw was lacking some sort of outfeed table.  This way I could cut an 8' long piece of lumber or plywood without needing help.  Since this shop is large enough, I built a decently sized one.  It's 4'x5.5'.  It was a left over piece of 5/8" particle board which I reinforced underneath with some 2x4s and made legs from a 2x4 ripped in half. On the bottom of the legs I attached screw-out felt feet. This way I could level out the outfeed table perfectly. This is how it turned out.

![](https://lh3.googleusercontent.com/g7JRj2BgKah7NuLKEpUjToZE_yRmEOUK_5z23f1n-LguiLp-6ogjwiGzsTlLmWEDfhwJIH1W_4AxALFXX5M9pk1WrPe7zlml_8apFx_RSbSLq-f9Hpk8HWczqgSxzSpxhujczF_O2jOxzmZrwyOp979tC8yza4a5kfs7wLpoaIwhkrge72uPbmNkD-HG32p-emqmmtDyuAxDekferd7dmjQHy9SIlpGcm0NiHXxloWpu_LpULq8TUm1tT2Fa0e4pkMyMSxfRKOM6VgkEL_ujeTGkPbs_7_Ynu-ccx0NUa1uv5fnP97X0gOiEjRFtu2jRDThnQGzewl8xIlkIyW1pMTSRl0Cok8ABpThR1fHezjIwL6CHaasnrXWFZ8KF5HRrWHpex1q-ri01pYFExfnUpsjlo7AWBRaT4MzEukYVLk1lac886c0KQaV-Se1xFZUA0ZPlUki2ZL6L-CfJFtlmN2yBhSoRprs3-6Ru_OoLoNdExtl347nw-sBF2_WbIC8VFnn_xmjVctnAodb0g_-tybvziNHW47MPRZSTUjGi3uA=w2979-h1676-no)

![](https://lh3.googleusercontent.com/arTfIwRLb3fcH6rhmhA6UKbezR2UhT6-DxZyYelFSmfycWXqgmXnFnlembWclAMDYRjgGQXrkPdBZWCmjJpntp8KRdK0SdXC5TdymlQX4TwO0NwWFWGWM_Kli3fH0CHbgjdHrknkKUiBhksYoOoY70_mWtFoDeAjXiaz4S0S8T6ktflGmjDPWkzD2voLNyGwCJHD4HIPlvNTu09GWtSZA5r8G7_ktck7STitO0EkeBYqasWHXMb8Rk6Zfi0F6YekFm2qEeEQY-IonRCUVCF6EtnUzRYHDtNGz4SfLGI6ipXu_yev3Sv3GG07NSBDEOnb4Zu65DY_R8tWN1DYITdSR2p0BAOwoyFg6vKwQc9ABRfcIfmvgws5qC3GwXw7KVKSuSNmC1hKTJKSITbT6YnkotbhCPl1HLl8ODNlch-nLBGdvQPDI_hacqK7EyouE-w6k9Hp28V97PEhA6u_SZpdjhl-Cz93pZqHDMds0579GPJALxyub_KseMf1uHmqMuDKPf9jhfhNT16PxidIlnVHhpNnUUw2ia_U8VK-fGrZmkk=w2979-h1676-no)

![](https://lh3.googleusercontent.com/57IwspFPGGivAdtQQoZAWpyLVG6ObLSWWAmL3346TK-5ymoBsOGYoI-0RWGqglcnsu7yDF5COA2hwPXDhr-psgcPsNSGA2MfOx9F0ieQ4czMPJVyjW_f0gNctsxYlW6oQzUQrb0EnMAGjFJECwNcWcNJLOMqmhT-QqQOM5Bw7y1_7FWDy0GEjrNICNuvHleeTrB_y6Sq3PyTZ2gerK5WDVMyYT2zjXbSLyM8lpr52Mfom7f28Kt2Yaho6Jh5i8sN1Na2GGwk58SWD7oytc8qo_1hca0fJXq1AdK7XxvYrhGeqsm1eflp7hVPgZRTD7-mtsN5-WdIpq1ydKfMTmnNkeiWErVxfJ2TVzWYj4x3SHX4i_PTQ5KUkzYrX0i2gaXVw4WN6-pjyU4hz5USqnqHAu-xQyTHEG3UqpOEgsOa9wzmlrYnQ1uPlCOcorRV0UDUZ1CSAmV8p4X_f3RrbhpPjO-v8TmrjZ1fQ2Aqm88oRpSrkXBf6tpIRyRK66b_cuqIXaCrz877Y82vavVoiH3DiLNU11ql3nnN3_fXC6TVJxU=w2979-h1676-no)

---

### Sewing desk

My wife, Kaytee, is getting an interest in sewing. She'll be getting a sewing machine in the mail soon and she needed a place to set it.  So I decided to build her a table where she can sew!  I decided on 28" high, which is shorter than the normal table/desk.  This is because the sewing machine will sit on top and add a few inches to the work area. I think I ended up making it an inch too short, so I decided to make 1.5" stilts from a 2x4.  You see those in the pictures. Now the desk has 2 heights!  It's about 18" deep and 3' wide. We bought some paint today so we'll do that soon! Check it out.

![](https://lh3.googleusercontent.com/pcbZ2lMdGSqABG3tmMtR-C90QiNDO85WGZd0x51yTjpVVQ4F98Ovus0dT2j4QW6qLKLsMO20ZX5bxVBKP-BhDHQ2PytVyt7ibRKwtddtDZa5pzviferRZllROw7-oS1nQoOwe3Mz-ORiJC1DlWLXAdaUV3GvQEM5z815agR9x20UE5X4O5bt107ZNv7WTGt-bt2mSKSNX6Te4eFJX_QhbhZk6E6Tk_OtlhYs7kj-KJhtOLG5sZ7mM8x1hn6yJPuzS6ooaso22ArlVBd6ooU1xiMNbmFij1DWKjoTyH4fMqweg9kxpU4HZS5vg1lIfT_PrMxmdMG5rjDFTvegxeXkg_Y705TYC2kPTUGMPpdPh1O6xb4GD4VJfSoE7Dsn5R9sSBhNj3Z7pc447ni5PRFMyBHOBNtsrK1r8QvsQ6078sZ6a_AerCDPkYXtciviITXDz1g8sxJ6VQHTzwHQ4IE5n5VLdWuvzoIVuQF3rHWWwUpXVzNegYcgyHUVeyxvTCvXlDHkmOmhov9IUL5a-250qkwgbTXapvdB5fbPBu1Qc-Q=w2979-h1676-no)

![](https://lh3.googleusercontent.com/exeNd30de3a1LJEWbOGBbpsEGm-kqX8Ax4ajwVrKl8pjH9MVh_BV6IjxFGBtdf9QNzY2DLg2qjq5sphNpR0W08EDjNQH0KZ15iqvil6Z1k3NJmNHIWSrDlFmENVLRU2ax7fPHZtwoIuBtBTznk7i7Tvyqe2ISqUYpsJvUSqkqhgHlMp41amRVEUIWTYHheUbG_ewq-Xe0I3FOina6FW2dqspLXWqtu0D23U0oRABFNLOy7ZiWE4UzZoIl988aOHp0RXP2kX9jmv0-Wj4NZNt-0d2FMAC18QAqmtwWgXM7Yy2u3mguxgYbrtEEGamtDogeyJFzoWOosr5cmMCgIZ_4Cz08QBBEm3cE1tIl41w1_r2V_ksgD8SuM0ogY0XZBBXrHNSmfsM297U5tGWSbVxa7x7RTV0SDABDVayKnFcpuoWGstFjVUE43vJ1-XjvZVVgCAFUfi6QbXT_i9HCUvy5MUd4W6OHiLI78kUsIR3HCWTaMoON3ZsDFh9Z1XH2PRuTFzdbVBB-cKFA0pxUyWBXXNS11g-1H_MFEaCzO6FNEE=w2979-h1676-no)

---

### Table saw sled

One other thing that my table saw was lacking was a crosscut sled.  I had some leftover walnut from my dad and I bought a half sheet of 1/2" thick high density fiberboard to make the bottom of the sled. I decided on a sled size of about 20"x30".  This will allow me to crosscut at least 16" inches with the baseboard and top board. I glued two pieces of walnut with some pine between for a unique look for the baseboard. The top board is simply from a 2x4.

The previous night, I cut, squared and glued up the walnut and pine.  The first thing the next day, I cut the HDF to 20"x30".  Then I took some walnut and made runners for the table saw slots. It is the hardest wood I have. Then I screwed the HDF to the runners with it all sitting on the table saw.  I used 3/4" screws.  Next, I glued up the 2x4s after squaring them.  Then I cut ergonomic curves in the walnut board after planing the squeezeout glue.  I sanded and routed a curve on the top of that board so it doesn't hurt to grab. I did the same to the top board (2x4s) but didn't route it.

The most important part of a tablesaw sled is making sure the sled will cut the stock at a perfect 90 degrees. So the next step was attaching the baseboard perfectly perpendicular to sawblade. I took my 2' framing square, held one edge against the sawblade and the other against the baseboard and screwed it from the bottom.  I only used two screws, one on either side. Then I'll do the test, which I'll talk about in a second. If it turns out to be far enough off, I'll drill a second hole on one side of the baseboard after moving it the appropriate angle.  The top board doesn't need to be accurate. It's just there to hold the two sled pieces together.

Finally the test. The idea is that you try to find the error in angle from the baseboard to the saw.  You can magnify the error by making the same cut several times in one board. I think [this video](https://youtu.be/ZtwK9X8o1Gw?t=7m12s) explains it pretty well. After doing the 5 cut method, I measured the top and bottom of the off cut.  I was very surprised with the results!

![top](https://lh3.googleusercontent.com/P5hFob4gj7dU1zYErHdGuXo2XhJPeCWHfZFEZDCdVQHQ0Nzm39dbIwrwR7A1gqbwFABSnRIu_-lEBkx9EXqVlNRz8tmhz5OBOsTb5U8utmXXAFxrnSJ4F4y0HHmuyEbKIEz80VewW8zggyqesE5Mw8zFraCbbGq-hJkr7WoECkyDG9N3VD6mABIlW5bInt8Yw5m_ZS94SaVpMD8Zqkpcn5qcKR_tAkzK-4WhgXavI3cm_zJ00ObOJ50tTKrye2KAY0mpM_hFZMGRoraLG3fIOgV3kdcnwCm2MDtbbGAr3ch3TmBuHEBvKwmmPeCsn1hBzUuUYU8sg1rVkzb4MgJ5IpzkyUeIPFxCtRze1Mo-tCsqA1giISaQON-CE0jLquV35mcEQXtozALCOtkMYtUL8guWQ3HF1tq7C2mlX6aysc_zjNpRXCes5RRAdFwc3aDaIllYqo3iLYwpokQC3k0fNoz28V_SL8Fi9-WpFD5AlQC4GxCS0GY4NGbGoaax14njeinRtvEnQcIhxN2pFBM77K9uBGOroh_eMvttKM7HFw4=w2979-h1676-no)

![bot](https://lh3.googleusercontent.com/6ECxZl_OEFrSF0MNQA-chHjSQYAs-A7Rqkeelf_LihDCExyojfRpAS6GimabFEgoG-5AM9_VCC3Aga6MKLYbf5-ZliUtM3q5XcxkswSCHbzhUF5ihZxGrsCxlTFrNTYwdtu5OLiTRuc-tyuqKlicDGVcXWnk9OOe0BNFNp44dW_uXk8xJ00gu_czcLKtqKAnsOhIZWC2pVxwMI7sGB73_sN7RBDfJr6HQ_ZoPaeKNByguqIaX2R7KWA5UEc_fSomF70NMqL3t0dkJzAaKe1OrCYhu0A1FhXh4hHKnKrZ8SyuZcpW2lrIWC1Ww9V7Y3ePS-LztxpITOwId9bfgIhj78CELkUh8XdUgmzPU87sFDkzW6QT_Qij7Aq9W50nC6Ql88s_VAeCpC09AcWAmYON5hk1h2_Chm4yPjFJ17jrEru6tFMiK9GGeG1F82dceDwxySciqpgM05mcmbjMCFXgj0aUjBS3PpLJ1mTWvNA7OEWXMD6mDc-Mppa_rXrwlfSA1CWJlAq6vXtV9x-e_WOBghtSTT15lyNx6Df9Ywy0Mn0=w2979-h1676-no)

After a cross cutting a foot-worth of material 5 times, the error was 0-1 thousandth of an inch!  I was quite taken aback.  Here is what the completed crosscut sled looks like.

![](https://lh3.googleusercontent.com/0VsTsO9ESVic3q7LKbgm63DlG7m3ObKzux0I9kmXxKFnsD9EdtPG1zj8tN3Jp8Ezuoi_49ewgQXXWDa155YzcFnhZ3jfQ1v2WrMJYMQSrFQ3Vjv7-oLEGGjOpq5uQb0ibUr2HElKdDvR84ct39el7y91TCo01jcMaDAGfJ-ym6kn0Ek9GeP0YCkYNKHHnJK5PmMBZYv_n0C8BiK2UjOWgdMrY3BOpblbMi3zeNrS-sgbimCcflxO6UdjPNxjMx9h_a_v0_0pS3scG-bKxitWiUATQMUo5W580kHPSGmhalOKYrOL-p2BWiugepsv0EyFOA6OgJctaDgvtU-jdErWl0dItMaZ16n5xpLvsTYDKZFnrCc_HnBujSMZA56o1OclzVNo1NkK-jvmyA9hTM_Z5tw7KY2vKKup2oNvchvxErpMkN4KdRc7scGWwdKdypNfTT0s2a0w-s5SsQiqJTxZoAlb9I8-LA7vF2AjYNHuS41qgd9kt4W1vK0X2SBruz2eN7bDXtvAepHVr2ifW6eLrI1NTp6osBbsWc5Pnllvf9I=w2979-h1676-no)

![](https://lh3.googleusercontent.com/HAAtz5v5GM8fjif8qm4sX_0-dy02Ss2z_V-OyxEidkye7Kyjiag7kG9Z81mjbmqS9oMdgJiQx1T0yrcoaWY4eiA36us1eYVSPNv9pwFpGqKkbz6dBfgXUYB2Kpb1CGPTq0epHXmi_MvHi8B63oC0KDaujzTGRH2W0AvKyq6c0NfjXpngdVmiaH7HVVi2njIOcW9kL5Hazu-rSuf1JmKHChxOK22CXRRAdV7l_bM0NrXiwLsVxtQSO-r5d30jBgvetPC8rd0lKtwqdAkqEvPun7_vXQBhObTfwlY8zRz5SRTFDtCRwS_g3lYKTOScYZCMqm7mLsEMWr-JIEY926Z9QQm09vUX8TvyEYCPi-KFeMn6DFYDyKVWa3D5SYxIIvT2h2qwBNNMViS-HzWzMo4g1kzLlzjc6h2rARA7JYmFJlBS9COtsi6x5qI-HKAiLWGY17hLwo2qf9SE4iBkNmbaTkqQBBLlmEpntLGhN9oVxue_aXdPjbA8ug3zAcUYbNKbsA2tEl004iLgnonWGSi1YWwLtXrRhswjdkjx4H5zOWk=w2979-h1676-no)

![](https://lh3.googleusercontent.com/fDibCkEBcSyG4cad9XkT0jKUR9Yc6O96NxhUpuYpAr7XHb0SsztNh0QDDKlOhmbSPhilQ2-Jibxuf0aV9LByX8xGGLxJNsm-W73ibCX-w3KM_y0AJ_Dt5uDsxQRUlpV6J3jGHBmuw60N-C_MOpEIfJUZD_0sd2VUB7tT9Jv-8luPkhzXypPrxs-HYxBYCsV4wQcw9ykV5mUM1zehrrC8shRbVGMwtb1COSQvs5H_JqVveLnXmxmVNDroJ3hJ6kSpkIWMPeSpS4LTP7nWF3UTcMubTKgfYLxJ-7FvtNzPE4qEKiBjmVPZIUvD5TTP-ruH7YMRVW8M2e9c29LE9OD-Y-9LOjGjxszoi-8fPB8UUMzc-_Y8B2wcxzkvhfo1iI1dZIJ27taSxF2H2y4ztgqSz3ChgyrjgrxR7U1UGX2PnairwaOVk6tPm9AVto4NPPNabInpK8XERBvlsQ3a8zQovQw0vjshd6JvJCVvwNAo-PAIBwQg8BCr-RHPdHt0jBIaDgyA19QSsYjBhM15L5rje4LdLVqKIcZ0funid4C1ICk=w2979-h1676-no)

It's been a great weekend of getting the shop rolling!
