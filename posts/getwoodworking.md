---
layout: post.liquid

title: "GetWoodworking: How I got started"
published_date: 2016-03-24 15:35:52 -0500
---
The [#GetWoodworking week](http://tomsworkbench.com/get-woodworking/) was created a few years ago when trying to answer the question: "how can woodworking be saved?"  Today, those of us who do woodworking are encouraged to blog, invite people to build things and help close the learning gap some when it comes to new woodworkers.  I decided to blog on how I got started in the hobby.

![GetWoodworking](http://blog.woodworkingtooltips.com/wp-content/uploads/2016/02/getwoodworkingweek2016.png)

I'll start off by saying I'm no seasoned, experienced woodworker, and I'd actually consider myself still in the 'starting' stage.  So don't expect a ton of deep groundbreaking knowledge, but maybe more of a story.

## Early Days

My early early woodworking days began back on the farm as a kid.  I remember drilling a hole through the front of a skateboard so we could tie it to a bike with twine and get pulled along the road.  One piece of advice from this activity that has stuck with me: wear decently strong gloves if you're being pulled on a skateboard.  A couple more early projects were fake guns and swords (pound two pieces of wood together at 90 degrees with a nail) and pinewood derby cars with help from my dad and grandpa.  Grandpa was the woodworker in our family, and he's the one I can attribute the origins of my interest in the hobby.

Grandpa has a woodworking shop in his basement and I remember at a very early age using the scroll saw to make fun projects. He never had the biggest or fanciest tools, but he'd often unveil some beautiful projects at Christmas time for the family.  I am now in my own house with my wife, and one of his bookshelves has found its way there though it was given to my family when I lived with my parents.

## High School

In high school I was in a few woodworking and drafting classes.  These classes were invaluable in teaching me much of the basic (and some advanced) lessons in woodworking. My first project in high school was a wall mounted coat rack with a shelf made of cherry.  I remember the wood choice because I thought cherry sounded unique.  It was less red/pink than I thought it would be.  Throughout high school I also made a swiveling entertainment center and my *capstone* was a beautiful walnut and maple recurve bow.

![bow](https://lh3.googleusercontent.com/bZfd1MwMssYV7RbFddQA6o10sr3Wup2hiZBieLaULKvUPmvrno6h6N9Bn0Fo6jnf4d4EZJTbd0EKnKeJxwkSO5RJsB5DUQH1B_B03B9vpKMgBoZHL0Mq0LH2Zd-ws_y-KK212diSYqZMQXYb76PbzC_93KOOxeftGLMvdeznLWxJFbbK6m7rSumlqqA-kk00b87XYB94SbHTvNKZTmHPDha5UmeY2y3VffOd014oT3aujW9i9iXyuoFoPDdaqffMKsvx8shtNZLVYlGAQWLuGaZ71EwEmx3q5VlEzuaGC0HsHePLFiE4BwGa3nr2xCoNJvfK5jD4OSV3CTgbJ5w5_ffAreUvMIVTmO1yCbw8CFgFoL2DDCqQd1wz3xjfdH9hW_8FJNgzeMU6Cs2ThFWxDYXSOw2Fc_LQAxk6yGq_CDpHfM8G61EtxDLrOsidBsR_bFZmW57gU2YzM6_7EO7bEKA0X9u2hWOX4sdzDIQ11aBgTMGeUYpWCMrCOnNALIap350W_FoGp2M1Uk6SO7rn3AooXbBUbscfDZe7Ps9wd8wxyCcVij-G_oxsk3XObaAs_nQn=w1079-h965-no)

Also while I was in high school I had a couple wood-related jobs.  As a sophomore, I helped my dad's cousin with building a 20'x35' add on for $6/hr the whole summer.  This definitely isn't woodworking, but it got my hands on some saws and taught me much about working with wood and many other helpful DIY things.  Another job I had was working at [Tomah Lumber](http://www.tomahlumber.com/), the local lumberyard in Tomah, WI. I was able to learn of the variety of materials, drive materials out to construction sites, load and unload wood and other construction materials, and even do some custom cutting orders for customers.

## College

Not much to report here.  During my senior spring break, my younger brother and I built an insulated sectional dog house for the puppy that was soon to be mine after graduation.  You can see some pictures of it [in this blog post](/posts/dog-house-heating-system-part-2.html).  I also built a riser for our apartment which gave us stadium seating in the living room which was a big hit throughout the building.

## Post College

This is where woodworking started to ramp up.  I began by building a few toolboxes while I lived in a rented property without much room or a garage.  After buying our current house, I had [room](/posts/new-house...-new-workshop.html) to move my hobby into a garage.  Before even closing on the house, I had a table saw ordered and it arrived 2 weeks after we moved in. That's when I really started building more finished projects.

Since getting my table saw, I've also gotten a chop saw, oscillating spindle sander, planer, and jointer for large equipment.  Except for the table saw, I was able to find all these machines used or free.  I use an app on my phone to keep an eye on craigslist and alert me whenever a specific search brings back a new item.  I think it's a good way to keep on top of local used equipment without hunting through craigslist every day.  I'm still looking for a drill press in good condition if you've got one!

## Inspiration

A lot of my inspiration these days comes from a number of YouTube channels.  There are some very creative and talented builders out there, and many combine their talents adding elements of electronics or metalworking into woodworking.  Here are some of my favorite YouTubers:

- [Matthias Wandel](https://www.youtube.com/user/Matthiaswandel) - an extremely brilliant (computer?) engineer who has taken his talent to the area of woodworking. If you like home made power tools, or videos on how to build very precise jigs, check him out.
- [Jimmy Diresta](https://www.youtube.com/user/jimmydiresta) - an East Coast man who knows how to use any material to build almost anything.
- [Paul Sellers](https://www.youtube.com/user/PaulSellersWoodwork) - an English woodworker who exclusively uses hand tools, and to great effect.
- [Matt Cremona](https://www.youtube.com/user/mcremona) - a younger woodworker who starts with retrieving logs from the woods and ends with some of the most beautiful furniture I've seen.
- [Frank Howarth](https://www.youtube.com/user/urbanTrash) - a woodworker with seemingly unending resources, a beautiful custom built shop, and an amazing knack for video editing.

I subscribe to a handful more, but these are the ones that resonate most with me.

Another inspiring thing is leaving useful creations behind when I'm gone. One might use the word 'legacy', but I have a long way before I'm there. The furniture, tools, and other projects I build are built with longevity in mind.  In a hundred years I believe the stools I'm working on today and the nightstand I work on tomorrow will be not only useful but hopefully also still beautiful.  Perhaps a grand child of mine will grow up eating cereal on the stool which I whittled on last night.  That definitely keeps me going.

Also old accounts of famous woodworkers for whatever reason are interesting to me.  I've only started reading one ([Joseph Moxon *The Art of Joinery*](http://www.leevalley.com/us/wood/page.aspx?p=71347&cat=1,46096,46112&ap=1) from the 1600s!), but I think I'll be picking up more similar books.  It's amazing to see where the craft has changed and how it has remained the same over decades and even centuries.

## Interested?

It's actually not a huge investment to get started in woodworking.  Obviously the big machines are great, but they are also loud, make a lot of dust, are expensive and weren't used in woodworking for centuries.  With a few hand tools, you can build some awesome and long lasting projects.  Check out [this playlist](https://www.youtube.com/playlist?list=PLqyeNiM0BJuVGykmrerIU_QXGRQiUbXwn) by Paul Sellers where he makes a clock for hanging on the wall with just a few tools.  It's a beautiful project and his videos cover the whole process quite well.  In another series, he [builds a whole workbench](https://www.youtube.com/playlist?list=PLD39949332C7FB168) with just a few hand tools.

Thanks for reading and remember to get out there and #GetWoodworking!
