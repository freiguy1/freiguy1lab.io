layout: post.liquid
title: "Pop can stove"
published_date: 2019-07-05 13:43:12 -0500
---

Sometimes I go down these rabbit holes and get obsessed for a week or two. This time, I started making camp stoves out of pop cans! I don't intend this post to be a DIY instructional but instead I wanted to share my 

I guess I found out about this concept when browsing reddit. I think on the camping and hiking subreddit, someone linked a video where a guy built his version of these camp stoves. I was immediately attracted to the idea because of the simplicity, low (basically free) material cost, good performance, and lightweight. I'll go over each of these.

**Simplicity**: All you really need to make these is a pair of scissors, a knife of some sort, thumbtack, and a little elbow grease. These can essentially be made with materials and tools sitting around the house.

**Low material cost**: Once you have the tools in hand, the cost to create each stove is just that of an empty pop can! I'll admit, I did a bit of recycle can diving to get some for free.

**Good performance**: With a bit of denatured or rubbing alcohol, you can boil water, cook some sausages or brats, warm up a can of vegetables, or other things. These can get pretty hot and stay cooking for 15+ minutes depending on the amount of alcohol.

**Lightweight**: For a use case which requires very lightweight gear for cooking, these rock. I'm not sure what a pop can weighs, but they're common enough that I bet you can imagine for yourself.

In researching this topic, I learned that there were many styles. I tried creating a few: inner capillary wall, outer capillary wall, and the normal inner wall style. Here are some pictures of my creations!

![](https://lh3.googleusercontent.com/Zb7UTu2iGsjT7WQGZlwUvcpM3-rNnKnmMtISgq55hODV_UK1ktiyQuTvoEUjwqNHGOPTLxJefcFvggFf5MnMg_ZLvWxEeYMvia5bZTaWpcjfYr5fBChkogKzE5UJf4eFX6afSnmpywgm92LEiHVIQjtQsKnwNKM2fYmP_9OvnLnRIzly3pcmXPO3dUZcikD7b3bjKMKKDAydOdhR26y8shVfGvFBCZWSqS9rqnxzw2Gg5vCv7mE0WufubxS0DPsIXrYYhIplYNoc46zoWxuVD8ds1ZYAUDWrbO5jTrMf1WUz3YdRa9CLDREG7BVsTwNDl5cRTUN7BfA9Ho-p3REfbNdyAUD4a8HJMDHdg0MKuEerx7W0CxG0uTSNQCZFs11Jg2AhCQ38m_eQ_VP2TYxf9Okby6tAG2FCbrTe_cfbBCj1mgc92cZSFnQlQhIiVzbOnyWJEMs-vza4b0V7s5fZZED3lyZc0Ehc6cw2qyIT-9WlLN_ztcHlIcjprRLRFAipbUftsqkymthc3b1MhJ0f0gS9BEDrF7yXc6fZ8lVLjfaAhOJRwhRle4iTXnG8T-R6oUmC56hXeaUeHdmiSp0sLmQjynPir2xHjUZFPOY0rL2jTOY4xc572B2DJJN-ca_OQuCYH4V_Apn75xVARAy5Ox-0LWzZEFzoFRbgArM4GwMGlXR51GGQ99s=w467-h362-no)

![](https://lh3.googleusercontent.com/sqRrYLod_aCIveN2VcuSz8sYayxcJtkyQwMwm65nOBN85uSU-jQ5FVeBFtuoxvBfYIp4ZTXcIIPe-BjAxwhiX8tiDA-tKyKECZZbAWXyvPE2gDTiAol5_R4jCeFYePpuF3RQ839xv7k6KguLUFlv6cswAZIBEuwd8L_Iyeb51sAw4lRW2CQkkwFeUApaXWStX6jQeg_nO15u5mwfdBMcylZe0-zX3ehaOOUcHDMj3eZ4hCJAQQscSodqnE3MLDbWh9NGQn3v94OhRLpyDC7m6IKJv8vy9hWHhn53vaorMxNx8DRMu9e227dECP_Xf1pFQP-jEEjI_CjtEMQ0fpdPxEXJWz7Dd1uCuWOKpBEDJni97LckVqcnuHUlKAyof1ioRi5kwJnckm6EqL8GJEyIZB1jm4QgB2bvxCwZOh4GhI4ZTBDGPj6_RmMj2k6WWnGfEfM4yuIzbhWzcdgjzbA1OgcPWfPrLy0WbXXSazTsgGjDGfPqtKfWfYpVtKYjsUFtE6Uso7RR6vI_JGYryKexpDBda0pkOz2fwEHOoSfJZbqeL740VZbJrNHjlI07_llt3hv4bZamQlqxATrdnKL19jHkMZma8WYCivx9SgEpwAK7vF12XzaUbtcOe3gzeWvNL5tzvpi8tfSqr4FYkgpRhK613z9SdMMN4px8X4G8sRFvddcwe7p2U50=w800)

![](https://lh3.googleusercontent.com/3rpMN4L_WRSDGTzJKmidukoxWOgWNXMbFBqXgVnJmKg3zH_5ihgPTIkbU1DaMhdT2d_Sx9NinZEYaWhbRzf050G_xvrHrKVe67XEsn02PPiFJhPmDOHKRDe8DBf_GIqE_7SUkesC6jRui92858UNL0Zc4LPNyXyP_Fc7aJNxGctgqb_mldIUq5Htrugrgj_6yXvgFJkqa92Cxs-u5vkFIo6ui-ARAPH99HbYRSEL_EhjzX9CvbrpeNG-LaU-ygBf24RnxTX9HeHZGQ2PmxGpw3h19JlUWsUsTq-eHC7XMPUKC0LW96bQKJndvYrJ6i5E5Oztpe8xa-01uLI2-RCPyZaZYWXCkjgb9jmpcZ3CjULik9MIB7fl6wuqyj_itLtFDBy8Bz3uJdPTE8xNU7Zu9hg5BVBkbLGQ-w0VI-U9-XRAU21oI4i2Ycnnbl1cFjgPuBm3-yFMwLyM9d-bKgOuxcQM5EDPkcqsvI8KtsZKWQAAFUlt6Tptv4vTyGV78h5I_aMdq6kvcdSNyN_NkgeSiYCKbF1sSX3XT1AHOZNzlBBWG1Hsm58XLBHGnxiEB84CHR46aovcfVwbR1n7nQowOv-W6-KDoe_MHQz3YqeL89JXOndkNjRL9fyxqSdw1kThbYvR9jLNVFDuaoURlVOjqSAXDGp0tpt1VOaLOTuWkcwpHDOuq0IPB3I=w800)

![](https://lh3.googleusercontent.com/uI22kP0aYAUmUGoIAAlv3pBdoUe-byi9Kzml22yrhLjOhCt7n1RTgukS5nxcMkLlcifH8qZW8GZul4NfiyztCmBf3puocuNx6gxfxdN-GxgwS-5rV_AR_Om6SlPNFCqyqGX8eqQfL4hhX3-akfW-wjx2et5jrTpYf03dky5K8oeD8ID76afPQliKHtp-bb0QFRO0DFDjLSNbLreZ49mJOYJxeSnKQwnzmxpHWemzPwSTZQfPtKAKlNTpLOFbL9oO1MeG-lSJHUqJ2ub3A2UUFUBYtii27yy0Ky8ARTh8ojqifreljSBG_3iJFIhqqlBxedjLh4znGNVzp7V-9qX1EVd8dEw4u2TnYURc9-O6NImWqb6IItbyVbsYG_LrynBXD2hIf_a4_fB6yenuiHkl2Sb4rDjbaNsfUYNE-MRtJq-5nGJDmSzL2Ygu6u9qhe81qEbDrveBqRgdYiE7bVyu9LCy_VQA9zHoBTUardt5vLM672fHD6CGhSN6PxXErvkKTBu8Upes52IZEPyX1PPt1L4ogBOOloLWa6Xr6FnRD0sEQxIC3klDbmjlM6edASVix6nwlQgbT4ChKgnRxpgb8-YSgmUUuSh8MttRd5cfjKuL9zm_SPHIrVcjupfthBkUqg2tCBSTaBdLACY_Im-o4fWT72j9uwSnAihOpHhpyi9fz17zj_lNPY0=w800)

I cooked some raw bratwurst using a little stove with a cast iron pan. They turned out very well!

![](https://lh3.googleusercontent.com/TW5lMOiiWCRxUs3bwO7Tn2ek6DXjxV558KUA5HExS9iDUqLGaz3jLBFha4xLrelQEObRJq7VlPZv8l6DphLwNWq1lcMqaP1J4bffYe5H0A_Zx5DZZFx8wGiDsdzL3LWREjVBYpAAs7-3LdT4mY0Lb0fNTTItpy4AcIklIWMpPjmTMFT4DyTyGgC1msDFXHG658gA4IpVMSm1Zcl7-mVJulejWm2qkv1BjT9OCJwjgLsNK1UMBthDu6jom8_YPbsDXW4wKg8-rTDYDlzvTx1DjuSmjLnzRIzm5Hjxrkt1hYflyQITylThgn_J0Ig0KycxfYuOdLPhD4a52Dg63kBsSmxl-a3dB8M9Ua5LNKmwpyNLbespy8FiyUoTFluYogIpU6gsoSLHawKK3x6n3zaKc8h4_ZyyY7xHmuZObo0KIVZtgqTNanj15u7USWcFPqlMmi22eMSODwsvcyqayygd41542ykNDNBzU5LRGZiS_iZldaNY34wF6GXgfH7UwJEx6sVPjlOu3L0U8Riybnl0vFYHfXjM9OV5ZapgwDCKeZ9gmKMyBWHbgAqfijlRePpkTSe4LRVoaiXCUTVDWnt42w3etepcDu0SFgchNAUTNdmWzBc47w9G4gZNI1XtIHh3yO-MPbnFqB3Y7pkRSY76EeF6eGG9Nx3L-dsgsJ2TFZHYgHq7OG2XZlM=w800)
