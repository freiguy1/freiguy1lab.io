layout: post.liquid
title: Urn
published_date: 2020-10-11 22:00:00 -0500
---

I was honored to be asked to build an urn for my Great Uncle Lewis, who everyone called Babe. The man was a smart guy, principled, and a woodworker himself. His wit would always catch me by surprise even in his later years. He told me often of stories about building his house by himself. You know, I wish more people were like Babe.

Anyhow, my instructions were to keep it simple, use red oak, and the dimensions should roughly be 9" wide x 7" deep x 11" tall. These instructions seemed doable. I started by looking at examples of urns, in box form, online. They're mostly pretty straight forward rectangular prisms. I started a design in Sketchup but then determined it was overkill. I was just going to make a straightforward box, and I'll add more decorative things near the end.

I wanted the corners to be mitered, but I wasn't sure if I wanted the grain running horizontal or vertical. With the sides being 9x11" and 7x11", it usually makes sense for the grain to run along the 11" direction. However, when making mitered corner boxes the corners should be end-grain which will prevent the boards from cupping or twisting. So I'd either need a really wide board (close to 12" wide) or fabricate one by gluing a few together.

I was very lucky to find a gigantic 11.5" wide red oak board that was nicely dried on my rack. This is a perfect use for the board. It was nearly clear of all knots and basically a perfect specimen. Also, I'm not completely sure on this, but I might've gotten this from Babe back in the day! Now wouldn't that be quite the connection!

I planed the board, cut all 4 sides to size with miters, used a dado blade cut groves in the top and bottom of the sides to accept the pieces which will be the top and bottom. I then made the top and bottom boards. The bottom was just simple pine. I chose pine because it won't be visible and it is light. The top was red oak which I cut to have a raised panel in the center. It actually protrudes above the sides when viewed from the profile.

With all 6 faces cut and sanded, I glued them all up with the amazing masking tape method. Next, I cut in slots for splines, glued some black walnut splines in, cut them flush and did some preliminary sanding. That revealed to me all the small gaps. I mixed epoxy with sawdust and filled any gaps. After sanding the extra dried epoxy way, I could finally cut the top from the bottom. I did this with the table saw.

Next was making the faux inner-wall which allows the top to mate to the box. I used some black walnut, cut them to size and glued them to the inside of the box. Then it was sanding time. I had several corners to break and I needed to sand away just enough from the inner-wall so the top slides on easily (but not too easily).

There are other pics and information on my [woodworking portfolio site](http://playground.ethanfrei.com/showcase/15).

[![](/img/2020/urn2.jpg)](/img/2020/urn2.jpg)

[![](/img/2020/urn1.jpg)](/img/2020/urn1.jpg)

