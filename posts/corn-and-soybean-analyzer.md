layout: post.liquid

title: "Corn and Soybean Analyzer"
published_date: 2012-05-17 13:16:45 -0500
---


## Background

This project has a bit of a history, so as long as you're in the mood for a story, lets get to it

It all starts with my dad, Dan. Dan's a farmer and every year he grows crops: mainly corn, soybeans, and hay for bailing. Well I found Dan going online every day, often multiple times a day to check the current prices for you guessed it, corn and soybeans. So I thought, "wouldn't it be lovely if he could just double click on an application and it would pop up with:

- current prices
- how they've changed since yesterday
- a graphical display that would show the trendline of the last 30 days

?"

 After many hours, this is what I came up with.


## Program Info

My first release would retrieve this info whenever you opened the app, so if you missed a day too bad, it wouldn't be recorded. You can obviously see how this would be a pain: if you missed opening or sort of refreshing the program every day, it wouldn't have all the data for you. So then your graph isn't quite as accurate. This program would store the data in a file on your hard disk every time it was ran.

Since then, to make a long story short, I've split that big program up into two smaller programs. One that retrieves/parses the data (used for server client) and the other that displays the parsed data (for normal user). The first part I've put on my server, and every day it runs every 4 hours, so the data is always fresh and ready to go. The second part retrieves the results (from my server) and outputs it prettily to a graph.

This program is written in java and uses some cool things. First it connects to the internet and downloads a page that contains info on the current prices, and this is retrieved in html format, so the second cool part is my parser that runs through this 500 line long html page to get just the prices; thirdly some magic happens with determining the date (as inter-month switches can be hard because of offset days). And finally I have a little tricky math that draws the graph. It's tricky only because I wanted to make the graph window drawn dynamically depending on the size of the window. As in: it's resizeable.


