layout: post.liquid

title: "State of the Ethan"
published_date: 2012-09-26 07:29:03 -0500
---

It's been a while since I've posted to my site. I thought it'd be a good idea to check in. Currently I'm still learning the Scala language with [Programming in Scala (2nd Edition)](http://www.artima.com/shop/programming_in_scala_2ed), and now I'm also taking a [free online class](https://class.coursera.org/progfun-2012-001/class/index) through Coursera taught by the creator of the language: Martin Odersky. Along with learning through the book and the course, I really like to see the application side of the language. So when learning a new language, I'll tend to create some apps that don't really do much.

The first one I started was a snake game written in Scala. I don't have the source shared anywhere as of now, but I might make it public sometime. However a compiled jar can be downloaded from **removed** (if you trust me) and invoked with the command `java -jar <path to .jar file>`. I didn't implement a game over, however collisions with yourself or the wall stops you from moving. I think there's a bug out there w/ your arrow keys not registering correctly in Windows. I developed this game in Linux, and didn't encounter this bug.

After doing some playing around with the snake game involving Scala swing, timers, graphics and a little game engine, I decided to tackle something different. That's when I started branching out and looking for work. An interesting hobby of mine (which is quite different from software development) is training hunting dogs. After training, a group of us will take some hounds to competitions. [Here](http://www.arha.com/little_pack_main.html) is the website for the type of hunt we compete in. So I had the idea that it'd be great to not only learn more Scala, but also try to fill a need. I decided to attempt creating a web application for the NKC ARHA Little Pack Division. 'This is currently in progress, and unlike the snake app, I'm putting this endeavor on **removed**. It's an app that runs on the [Play! Framework](http://www.playframework.org/) (which I am currently also trying to learn) written in Scala. Currently, I've implemented the ability to log in and out, which seemed like a big step. It's not that secure atm, but it's a base to start with. Here are a couple screenshots:

**removed**

**removed**

As of now, there's a pretty nice database design behind it, and I like the direction we're going!


