layout: post.liquid

title: "Welcome!"
published_date: 2012-05-08 09:51:47 -0500
---

Welcome to my new WordPress-based website! I'll be converting my old custom-built website (@ [ethanfrei.com](/)) to use this nice open source content manager. It's not only to make my life easier when �updating the site, but also to learn the inner workings of WordPress so that I can add this great piece of software to my tool box.

My old site is being hosted on an [Ubuntu](http://www.ubuntu.com)�virtual server that lives on my home desktop. �That�s not optimal because

- My desktop needs to always be on. �Without any�efficiency upgrades/modifications this machine sucks some power ($) due to the larger power supply and graphics card.
- All traffic uses my already-not-amazing bandwidth and makes it worse.
- I use my desktop for the light amount of gaming that I do. It's nice to have all my CPU and RAM available to game without resources being allocated for my web server.

I�ve instead moved to using an [Amazon EC2 server](http://aws.amazon.com/ec2/). �The cheapest version is currently free. It's running a t1.micro processor with 613MB of ram and 8GB of hard drive space. �Sounds pretty underwhelming (pretty sure my phone�s more powerful), but with a snappy LAMP based server, it's plenty for a server without much traffic. �I�ve installed a number of software packages that make it convenient to work with. �One of which I�m pretty proud of: a vnc server so I can do server administration via a GUI (using the [fluxbox wm](http://fluxbox.org/)) when needed.

Thanks for stopping by! I can only hope I have time to migrate the site quickly, so stay tuned. �Also I can�t wait to shut off my desktop at home.


