layout: post.liquid

title: "New thing: Backpacking"
published_date: 2013-07-19 09:11:46 -0500
---

As if I didn't have enough hobbies, a new one has been added to my list this summer, and that is backpacking. I've never officially backpacked yet, however my friend Eric and I do have a plan to hike 15 miles over 2 days through the woods in the [Chequamegon Nicolet National Forest](http://www.fs.usda.gov/recarea/cnnf/recreation/camping-cabins/recarea/?recid=27781&actid=29). To be more specific, we'll be doing the southern section of the Ice Age Trail that runs through the Medford district of that forest. This is scheduled to happen 8/2/2013 - 8/4/2013 and will be a great way to test our equipment. Speaking of equipment, let me give you the rundown.

**Equipment**

- <span style="line-height: 15px;">Backpack an obviously important piece of equipment [TETON Sports Explorer 4000](http://www.amazon.com/TETON-Sports-Explorer-Internal-Backpack/dp/B001947FG8)</span>
- Sleeping Bag [TETON Sports Trailhead +20 deg F](http://www.amazon.com/gp/product/B007JTLKCC/ref=oh_details_o01_s00_i00?ie=UTF8&psc=1)
- Sleeping Pad [ALPS Mountaineering Self Inflating Pad](http://www.amazon.com/gp/product/B001LF3FQE/ref=oh_details_o01_s01_i00?ie=UTF8&psc=1)
- Pillow [Therm-a-Rest Compressible Pillow](http://www.amazon.com/gp/product/B000HX3UJS/ref=oh_details_o00_s00_i00?ie=UTF8&psc=1)
- Cookware [Texsport Black Ice](http://www.amazon.com/gp/product/B000P9F1EQ/ref=oh_details_o03_s00_i02?ie=UTF8&psc=1)
- Stove [MSR Whisperlite Universal](http://www.amazon.com/gp/product/B005I6P2VS/ref=oh_details_o02_s00_i00?ie=UTF8&psc=1)
- Water Filter [LifeStraw](http://www.amazon.com/gp/product/B006QF3TW4/ref=oh_details_o03_s00_i00?ie=UTF8&psc=1)
- Water Container [Nalgene 32 oz.](http://www.amazon.com/gp/product/B002PLU912/ref=oh_details_o03_s00_i03?ie=UTF8&psc=1)

**Future Equipment**

Tent: I've been looking into a few and here are my criteria: under 5 pounds total weight for a 2-3 person one. Under 3.5 pounds total weight for a 1 person. Under $200. Lifetime warranty (subject to company's discretion).

- [Eureka! Spitfire (Solo)](http://www.amazon.com/Eureka-Spitfire-Tent-sleeps-1/dp/B000EQ8VIS/ref=sr_1_1?ie=UTF8&qid=1374167267&sr=8-1&keywords=eureka+spitfire) (3 lbs 4 oz)
- [Eureka! Spitfire 2](http://www.amazon.com/Eureka-SpitFire-2-Tent/dp/B001EXSCOS/ref=sr_1_sc_1?s=sporting-goods&ie=UTF8&qid=1374167354&sr=1-1-spell&keywords=eureka+spiutfire+2) (4 lbs 8 oz)
- [Eureka! Amari Pass 2](http://www.amazon.com/Eureka-Amari-Pass-2-Tent/dp/B00AA25THW/ref=sr_1_1?s=sporting-goods&ie=UTF8&qid=1374167392&sr=1-1&keywords=eureka+amari+pass+2) (5 lbs 1 oz)
- [Eureka! Amari Pass 3](http://www.amazon.com/Eureka-Amari-Pass-Tent-3-Person/dp/B00AA25TRM/ref=sr_1_1?s=sporting-goods&ie=UTF8&qid=1374167406&sr=1-1&keywords=eureka+amari+pass+3) (5 lbs 6 oz)
- [Sierra Designs Light Year 1](http://www.campsaver.com/light-year-1-tent-1-person-3-season?gclid=CNjNqJPUu7gCFY87MgodgisANA) (3 lbs 6 oz)
- [Kelty Grand Mesa 2](http://www.amazon.com/Kelty-Grand-Mesa-Backpacking-Person/dp/B004LF64QY) (4 lbs 10 oz)
- [Alps Mountaineering Zephyr 3](http://www.amazon.com/ALPS-Mountaineering-Zephyr-3-Person-Tent/dp/B00AU6CYSA/ref=sr_1_1?ie=UTF8&qid=1374241783&sr=8-1&keywords=zephyr+3) (5 lbs 7 oz)
- Dog Stuff because then Bo can come and carry his own weight! - Collapsible bowl
- Dog backpack

As of now I'm set to go if I borrow Eric's extra tent. I'll just need to carry a few extra things for my dog. Hopefully this develops into a long term hobby I can enjoy with friends and family for the next many years.


