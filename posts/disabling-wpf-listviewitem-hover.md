layout: post.liquid

title: "Disabling WPF ListViewItem Hover"
published_date: 2016-02-19 19:12:48 -0500
---
I spent the last couple days trying to build a very minimal and simple grid in WPF.  There was no style to it other than the headers were underlined text.  No grid lines, no header background color, no hovering triggers.  This proved to be a difficult task.  Here was my end goal:

![](/img/2016/wpf-table.png)

I started with a DataGrid, but soon learned that I was getting nowhere with it. I moved onto a ListView containing a GridView.  This showed more promise so I kept using it.  I couldn't simply use an ItemsControl since we wanted the columns to expand to the width of the content.  In other words, the widths for each of these columns could be different, and several of these tables gets displayed in one view.

One of the biggest hurdles was getting the rows to not highlight when hovering over them.  A few Stack Overflow questions attempt to address this, and [this one](http://stackoverflow.com/questions/27042163/how-to-remove-hover-style-for-listview-rows) seemed to be the most applicable.  However updating the two brushes didn't work for me.  The change which fixed it for me was more... involved.  I ended up finding some default syles for ListViewItem [here](https://mui.codeplex.com/SourceControl/latest#1.0/FirstFloor.ModernUI/FirstFloor.ModernUI/Assets/ListView.xaml).  I modified it and ended up with something like this which lived in `ListView/ListView.ItemContainerStyle`.

```xml
<Style TargetType="{x:Type ListViewItem}">
    <Setter Property="Background" Value="Transparent" />
    <Setter Property="BorderBrush" Value="Transparent"/>
    <Setter Property="VerticalContentAlignment" Value="Center"/>
    <Setter Property="Template">
        <Setter.Value>
            <ControlTemplate TargetType="{x:Type ListViewItem}">
                <Grid Background="{TemplateBinding Background}">
                    <Border Name="Selection" Visibility="Collapsed" />
                    <!-- This is used when GridView is put inside the ListView -->
                    <GridViewRowPresenter Grid.RowSpan="2"
                                          Margin="{TemplateBinding Padding}"
                                          HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}"
                                          VerticalAlignment="{TemplateBinding VerticalContentAlignment}"
                                          SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}"/>

                </Grid>
            </ControlTemplate>
        </Setter.Value>
    </Setter>
</Style>
```

I altered the default style to not include any triggers, and this fixed the highlighting hovering issue for me.  

Hopefully this is of use to someone out there struggling to make a deceptively simple change to a grid.
