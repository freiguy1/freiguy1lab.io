layout: post.liquid

title: "Theobald - The Self-Balancing Robot"
published_date: 2012-05-17 23:19:52 -0500
---


## Team

Zechariah Utecht, Anthony Smith, and Ethan Frei


## Introduction

The idea of this project was to design and construct a robot that consists of a body and two wheels that would be able to balance on its own. This behavior is similar how a segway (a new mode of transportation) works. Our goal was to have a working finished product a robot that, without human interaction, could balance on two wheels.


## High Level Physical Design

The design of this project is relatively simple at first glance. Physically we planned to have a body, which would be made of two platforms separated and held in place by four threaded rods. The bottom platform would simply be for holding the wheels in place. The top would hold the microcontroller and supporting hardware (which will be addressed next). The weight near the top helps keep the robot more stable, so the wheels can correct for tipping more gradually.


## High Level Microcontroller Interfacing Design

In order to make Theobald balance, we need the two most basic things in computing aside from the microcontroller itself input and output. Our input consists of something that can sense what angle the robot is at in relation to the ground. A few things can do this: we could find distances from a side of the robot to the ground using infrared distance sensors or supersonic range finders, or we could use a combination of and accelerometer and gyroscopes to get the angle in relation to the earth and correct that way. Then we also need output. This comes in the form of our two motors which will attempt to right the falling robot. To control two motors without creating our own [h-bridge motor controllers](http://en.wikipedia.org/wiki/H_bridge) we need a motor driver with the following capabilities: interface with our microcontroller, output to two motors, and be able to go clockwise and counter-clockwise.


## Member Task Distribution

This project was done in a very collaborative manner. The main construction of Theobald (as far as the frame) was completed by Ethan since he was actually in the process of building a doghouse and had all the equipment and materials at hand. After the frame was complete all members collaboratively assembled pieces, soldered, researched, developed, implemented, tested and re-implemented. After finishing, the bulk of this webpage was completed by Ethan while Zech and Tony worked on our presentation.


## Hardware Microcontroller ([Arduino Uno](http://www.arduino.cc/en/Main/ArduinoBoardUno))

The microcontroller we used is the Arduino Uno which is built on the ATMega328 microcontroller. Arduino builds the board that uses this microcontroller and gives it useful interfaces like a power jack, built in 16MHz clock, USB connection, and convenient spots for hooking up wires. Visit the link to get all the specifics with the Arduino Uno.


## Hardware IMU ([SEN-10121](http://www.sparkfun.com/products/10121))

This accelerometer/Gyroscope combination (often called an Inertial Measurement Unit or IMU) is very convenient in that it combines two nice devices and uses a serial interface for output when requested. If the output instead was analog, it would be up to the ATMega328's analog to digital converter to get a digital value which has only 10 bits of precision. Instead, the gyro outputs in 16 bits precision and the accelerometer in 13 bits. Using an accelerometer or a gyroscope alone has their drawbacks. The accelerometer reacts wildly when it gets jerked. Because of the way the component works, when jerked the accelerometer will output a very modulating signal until it gets back to equilibrium. The gyroscope, on the other hand, is prone to a drift factor. After the gyro has been moved, it will slowly drift back to its equilibrium point. Using these two devices and a mathematical algorithm/filter called a[Kalman Filter](http://en.wikiped.org/wiki/Kalman_filter), one can combine the outputs and get more of an accurate output. The Kalman filter is a complicated mathematical method and an explanation of it can be found in the link.

Hardware Motor Driver ([Dual TB6612FNG](http://www.sparkfun.com/products/9457))


## Software Design

The way we designed our software was in a very step-by-step (then loop and do it again) approach. Initially all the setup needs to happen. For setup, methods were made to get the IMU and the motor driver rolling. After these were complete, our program starts its loop. Raw values are taken from both the gyroscope and accelerometer. Then these are converted into useable values. After conversion, these values get passed through the Kalman filter to get our three degrees of freedom as reported by the IMU. Finally one of these degrees the only one needed to detect what angle the robot is leaning is used to send the motors their direction and speed. If we had had our way, this degree value would be sent to a PID algorithm (starting with just a proportional constant, then adding a derivative constant, then possibly an integral constant if needed) which would give us a more stable design. However complications happened and we weren't able to implement the PID algorithm. These complications will be listed later in this document. For language specifications, we used Arduino's language which is very C-like, and nice to use. Most of the more difficult hardware implementations are created already for us including interrupts, sending out analog or digital signals, serial protocols, timers and more. We were able to use Arduino's `SoftwareSerial` library which made communication with the serial interface of the IMU very convenient.


## Results

Our results were overwhelmingly positive. We were able to get correct from the IMU within the session of coding, send speed to the motors based on the position of a potentiometer during the second session, and through the third and fourth sessions, we were able to get the two connected. However, we encountered one major road block. While delivering a program to the microcontroller, something happened: a wire got shorted, the computer we were using shut down, or something to that effect, and we could not write anything to the microcontroller again. It seems as if something fried with the USB module on the Arduino Uno board. The last program that was on it can run perfectly, but no new programs can be uploaded to the flash memory. The last program that was running on the Arduino simply directly mapped the degree output to the wheels. We just wanted to test that to see if -1 degree would make the wheels turn backward slowly and so on. This did work, and ends up being our final product until a new Arduino could be purchased to fill in where the broken one has left off. Unfortunately we did not have the time needed to buy a new microcontroller, or our product would have been working much better than it does now. On the positive side, we were able to get the basic implementation completed and show how the accelerometer, gyroscope, microcontroller and motors/driver can be used together.


## Conclusions

A word of advice we have as a group is make sure your connections are good on your bread board so wires do not accidently touch that are not supposed to! The Arduino Uno is a very well made and robust hardware environment, but anything is prone to damage without proper care and caution. This aside, had we been able to upload more refined programs, we probably would have still ran into a few more problems. The robot, as it is seems to lag just a small bit. What we mean by this is that once tipped, the motors do not come on right away. This makes sense, as the microcontroller is doing many operations and calculations before being able to send the motor driver a signal. If this was large enough of a factor, we maybe would have to look into streamlining our implementation to make it as speedy and lightweight as possible.

<div style="text-align: center;"><iframe allowfullscreen="" frameborder="0" height="356" src="http://www.youtube.com/embed/d4TMuDNO9Pc?feature=oembed" width="474"></iframe>

</div>
