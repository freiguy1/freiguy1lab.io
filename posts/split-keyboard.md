---
published_date: "2024-10-30 22:56:49 +0000"
is_draft: false
title: Split Keyboard
layout: post.liquid
---

Do I address the elephant in the room of not writing a blog post for 2 years? Naw.

On a 2 hour drive home, I listened to [a podcast](https://changelog.com/podcast/608) about ergonomic keyboards, specifically split mechanical keyboards. Apparently that podcast planted a seed which would take me down the rabbit hole of researching, buying and building my own split keyboard: the Sofle Choc.

![Keyboard Picture 1](https://lh3.googleusercontent.com/pw/AP1GczMtRrujL6LotHIZGCfRKQgW8e7IN9mRvmYi48zGrKe-3AizlUSCWLSRi8cNqEzcW-k9AMcIH97iPY15vxbRSLrJ9urv-V47oRz_1A9gxke9nL1VGp797bp04ih-FWVC0oiRmHn1P4EJx6ZDEgYOzyggwQ=w1200)

![Keyboard Picture 2](https://lh3.googleusercontent.com/pw/AP1GczPB3dXD7Rs2nx-9-hxO6tFucJ_JNLlio7NxhVeFVma9OO804-6BfB0t0Fs_sYnHXmRPulH3bxYZcVgkNHXvBiBQEh8PKKvoZqeg_T2NZ0H4sv6dRkOOcus0elEyiR1aRKkAd_ym1OjcoE7OLEUIti0ATg=w1200)

![Keyboard Picture 3](https://lh3.googleusercontent.com/pw/AP1GczMdXi2hXBejj1k9zjvIRaGBAQMB0BPEimm8HO7KGzAayNuG0jlwc89myajKzvMYm15_UzeXitfYXlN1hdyR0Mbg87D8Qqvd27GRTjaUDTG8Vzn_E5m3_-8UPEhJIffCup5SgFaBgkY3R9FyTCjN2rnwGQ=w1200)

![Keyboard Picture 4](https://lh3.googleusercontent.com/pw/AP1GczO6tNA1aHv80dp9NnNmZaJAf3LsqW_HFOK3UPvj_WjTWPxnyDTtZhp4qEqD21PTNsxIS94f5YYJcFmk-gW0Z17fUf0z8tIanVuesP31SDLuvQWVrnltFA6g5ceeWYoLGgWdmOydwsum1GjzGmDrg7hnyg=w1200)

## Materials
1. Sofle Choc kit from Keebd $63
2. 2x Helios Microcontroller $36
3. Microcontroller hot-swap low-profile headers $12
4. RGB LEDs $9
5. 70x Kailh Choc silver switches $18
6. MBK Glow R2 Keycaps + some extras: $60

Grand total: $198!

## Okay, spendy. Why?

A few months ago I never would've dreamt about spending nearly $200 on a keyboard. Especially one that came as pieces - requiring soldering, assembly, and flashing firmware. There must've been some pretty big motivations!

**Ergonomics:** So this is an important reason many people get into the split keyboard hobby. Their fingers, hands, wrists, neck, shoulders hurt from using a conventional flat one-piece keyboard. This wasn't really me. I've never experienced pain in my hands, wrists or fingers from typing. Maybe some in my neck/upper-back due to long days at the desk. This wasn't a hugely motivating feature of the keyboard for me. I did want to experiment with a lower profile so my wrists didn't need to flex upward as much as a high keyboard requires.

**Build something:** Honestly, I haven't built many things this year. In January I built an oak bookshelf. I haven't really built any wood-based or software projects since then. I was kind of itching to create. I can chalk this one up as a motivator. It's plain fun.

**Customize:** I love customizing things. Being able to pick out each part. Go with my preference. Create something that noone's ever created before. I've done this with guns, computers, operating systems, and now keyboards.

**Everyday tools:** I have this opinion that if you use something every day, it's fine to invest more than the average person for high quality. Carpenters' hammers, butchers' cleavers, chefs' pans. I was using a good keyboard! I received it as a gift, and it's served me very well. I was ready to take it to the next step. I guess this is another angle that helps me justify the cost.

**Geeky:** Soldering? Check. Customizing embedded firmware? Check. Flashy RGB lights? Check. Ultimate conversation starter? Check. Sometimes it's just cool to have cool things. With my somewhat limited electrical engineering and firmware experience, I thought this a perfectly geeky project to test some of those skills.

## The process

I watched many YouTube accounts of other people who built their first keyboard, and for many it was painstaking. Soldering dozens, maybe hundreds of extremely small components for hours; all to plug it in and have it not work. Then it was time to troubleshoot, ask questions online, get out the multimeter, and generally get frustrated. After another several hours, maybe they'd have a working keyboard.

My experience was different. I absolutely loved the assembly process, and it went fairly smoothly. After soldering 60 diodes, 60 hotswap sockets, 60 RGB LEDs, and a few other components, I plugged it in. I could hear the drumroll in my head... The RGB LEDs came on, but not all of them. I had a solder-bridge on one, fixed it, and they all worked! Next I was typing, and the keys seemed to work! Eventually I discovered a row which wasn't working, and examining the soldering revealed that I had missed soldering the second pads for the switches on that row! I just skipped it. Simple fix, and the whole keyboard was functional!

The firmware process went decently too. I borrowed some configuration (keymap) code from this keyboard creator, tweaked it, built it with the qmk docker container, flashed it to the Helios, and in general, it just worked! [Link to code](https://github.com/freiguy1/qmk_firmware/blob/master/keyboards/sofle_choc/keymaps/default/keymap.c). I'll probably be making tweaks as I learn what I like, but I'm just getting used to all the layers and keys that I currently have.

## Firmware/keymap

Here are some features of how I've configured the fimware on the keyboard. Primary layer is a qwerty keyboard. Thumb keys are space/enter, layers, and ctrl/alt/win/option/cmd (depending on OS). Left thumb layer reveals mostly shortcuts on the left hand and arrows/cursor manipulations on the right. Right layer brings F-keys and additional symbols. However one of my favorite parts is the "modes."

At work I use a Macbook, and I remotely connect to a Windows development machine. At home I use Linux for programming (and writing blogs!) and some Windows for gaming. I wanted a keyboard that felt natural in all those disparate environments. One thing I was hoping to solve is unifying the commands for common shortcuts I have: undo, redo, cut, copy, paste, find, home, end, next word, previous word. I believe all these are different keys or key combos from Mac to Windows.

This firmware has a Mac mode and a Windows mode. The keys emit different keyboard commands depending on the mode it's in. Say the keyboard is in Mac mode. When I want to move the cursor to the right end of a line, I'd hold Cmd and press the right arrow. Holding the left layer key and pressing the period button will invoke that key combo. When in Windows mode, that key combo will produce the End key.

The Mac mode also sets up my thumb keys in the order they appear on a Mac keyboard. Left hand left-to-right: ctrl, option, cmd. When in Windows mode: ctrl, win, alt. This way it feels natural depending on the OS I'm using. This may not be everyone's preference, but it works well for me.

## TODO

I have not completed the build! The kit came with two OLED screens which fit nicely over the microcontroller and can display lots of info like RGB mode, OS mode, and whatever you'd like to customize the firmware to display.

I've also not yet installed rotary encoders (dials). I'd love to have them for volume and scrolling.

Last thing to improve is cables. I'd love to get orange cables that match the accent color I've been using. I just don't want to spend $50+ more on cables, so I'm hunting for a deal.

Dec 12, 2024 update - A coworker printed some low-profile cases for me. The keyboard sits in them like a bath tub, and then they're secured together with heat-set nuts and screws. You can see the black cases in the lower 2, 3 and 4th pictures. I think they work and look great!

## What I'd do differently
I'd buy a cheaper microcontroller board, but probably stick with the RP2040. Adafruit has the KB2040 for $9, which would've been half the price.

