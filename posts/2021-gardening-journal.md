---
published_date: "2021-04-09 22:56:49 +0000"
is_draft: false
title: 2021 Gardening Journal
layout: post.liquid
---

This year my garden is more ambitious than ever before. I wanted to write a blog in journal form documenting my experience. I'll start by going over how I gardened in the past, then look at the plans for this year. I'll also attempt to keep this updated as the gardening year progresses.

## The Past

I built a raised garden bed in 2018. Each year I've used that for some veggies. I've planted tomatoes, cherry tomatoes, peas, cucmbers, flowers, peppers, broccoli, carrots and probably some other things in that raised garden. However the plants are ultimately stunted due to the limited depth of the soil. It's about only 10" deep.  As you might imagine, carrots especially had a hard time.

![empty raised garden bed](/img/2021/raised-garden.jpg)

![raised garden full](/img/2021/raised-garden-full.jpg)

Last year I decided to add on utilizing part of the garden in the yard. This meant reclaiming that beast. It was overgrown with grasses, stinging nettles and a hundred other things. Even a tree had taken root, and my ATV was hardly up to the task of pulling it out. I tamed about 1/3 of it and planted 12 tomato plants, a couple brussel sprouts and jalapeno peppers. What happened blew us away - the plants (the tomatoes especially) grew amazingly. I was able to harvest hundreds of cherry tomatoes and dozens of large beefsteak tomatoes.

![2020 tomatoes 1](/img/2021/2020-tomatoes-1.jpg)

![2020 tomatoes 5](/img/2021/2020-tomatoes-5.jpg)

![2020 tomatoes 2](/img/2021/2020-tomatoes-2.jpg)

![2020 tomatoes 3](/img/2021/2020-tomatoes-3.jpg)

![2020 tomatoes 4](/img/2021/2020-tomatoes-4.jpg)

This year I plan to do much more and if the harvest is half as good as last year, I'll be satistfied!

## This Year's Plans

After having a lot of fun watching the garden grow and flourish in 2020 I've decided to use the whole garden in the yard. I measured it at 24'x14'. So it's by no means a huge field, but I'll certainly have my hands full. We're planning on growing tomatoes, cherry tomatoes, sweet corn, onions, carrots, potatoes, cucumbers, peas, peppers, bush beans, and radishes. Last night I worked on an epic garden map on some graph paper. Keeping it all straight is tricky so this map will help a lot! Click on the map for full resolution.

[![garden map](/img/2021/garden-map.jpg)](https://lh3.googleusercontent.com/pw/ACtC-3c1mTsJKClfShUc9PGaX6q165zakUkY_szrBRdNa-yvNL1-w-572QNk4qNXUNMi1fcsTxGJfBy6cPnB1VF9gs8TRy0TAnzg4ULsRw8QDYx6OJiX6GkNa-yNLl_qTRgniPXlT1CdA8xkjd-cJ1qKZe4ENQ=w3950)

## Intermittant Updates

### 4/9/2021

I've started 36 plants. Most of them I started on 3/14 which might've been a bit early. The tomatoes are about 3" tall, and the peppers are 1" tall. Marigolds have had trouble coming up. I just replanted 8 cups with 5 seeds each four days ago. Two of those new marigolds have popped up today.

![started plants](/img/2021/started-plants.jpg)

I worked a lot on eliminating grasses and roots from the garden. I also covered a good fraction of the garden with cardboard weighted down by landscaping bricks last fall. The intention was stopping any new grasses/weeds from growing before we were ready to till. Then my dad came up with their nice tiller. Now the garden is looking like a beautiful black fluffy bed, and it's ready for some plants. It might freeze a time or two yet though, so I'm waiting for the end of a April most likely.

![tilled garden](/img/2021/tilled-garden.jpg)

### 4/14/2021

Every cell of the 36 cell planter has a plant started! The tallest tomatoes are approaching 7 or 8 inches. There are 9 marigolds growing now, and we may give some away if we don't have room for them all. I also worked on making row stakes from cedar in my workshop. I made 22 stakes, and I filled the garden with 16 of them using my map above. I had to modify one thing: switching from four rows of corn to three. With four rows, they seemed too close at 8". With 3 rows, they stay a foot apart.

![garden stakes 1](/img/2021/garden-stakes-1.jpg)

![garden stakes 2](/img/2021/garden-stakes-2.jpg)

In the picture above, you can see a couple trellises I made last year for peas! They worked well and I'm excited to use them again.


### 4/22/2021

Well the trajectory of gardening good fortune has taken a turn for the worse. But I guess there is some good news as well. Let's talk about the bad news first. A lot of the started tomatoes and peppers are not looking good. I bet only 2 of 6 peppers are still alive and just a couple tomatoes of 15.  I'm not exactly sure what happened. My best guess is that the plants got too big for their little starter cups. Two nights ago I transferred all the ill-looking plants to bigger plastic cups with some extra dirt. So this is what we have now:

![dead started plants](/img/2021/dead-started-plants.jpg)

On the upside, my dad said I could plant some potatoes and peas already. So I have planted 8 potatoes and a handful of peas! I think I planted those on April 18th. Peas are 2" apart in a few rows and potatoes are 1' apart in one row, planted 8-10" deep.

### 4/28/2021

On 4/26 I planted one row each of carrots and radishes. I did this despite my children's best efforts to stop me, ha. So now I plan on planting another row of each 1' away in a few weeks (5/17 ish). This way we have a couple harvests of each.

I've officially pronounced a bunch of plants dead, so I replanted some yesterday, 4/27. I think it was about 3 yellow peppers, 4 beefsteak tomatoes, and 3 cherry tomatoes. This is much later than usual, but ya know the packets said I could harvest in like mid July if I planted today, so I'm trying it!

### 4/29/2021

Not much has changed about the garden. The one small thing I accomplished was placing tomato cages. I'm not sure if I'll leave them or just use them as markers for tomato locations, then remove them until the tomatoes grow up a bit. I'm sure I'll let you know.

![tomato cages](/img/2021/tomato-cages.jpg)

Also I thought it'd be neat to take pictures of my little family _and_ the garden to watch them grow throughout the summer. Family garden portraits, take one!

![fam 1](/img/2021/family-garden-1.jpg)

![fam 2](/img/2021/family-garden-2.jpg)

### 5/4/2021

We've got some garden plants popping out of the ground! I've spotted a bunch of radishes and five-ish peas. I think the first time I saw them was 5/2. That means the radishes were up in only six days! Here's the row of radishes.

![radishes start](/img/2021/radishes-start.jpg)

We received some sunflower seeds from Grandma Judy for May Day. I've planted four of them inside and we'll see if they can take off!

The tomatoes that I re-planted in red solo cups on 4/27 have all come up! Five tomato plants in total. Not one pepper has pushed through yet but I'm optimistic. I think that's everything.

### 5/12/2021

On 5/10 I planted onions. I think only about 10 of them, but it'll be something. I still haven't seen any carrots pop up yet, but I think you have to give them some time. Also the potatoes haven't come up. Radishes and peas are looking good.

This spring has been cold and a week into May we're still having freezing nights. As of today though, I think we're in the clear. Because of that I moved one tomato outside. It was a cherry tomato plant that was growing like crazy inside. I bet it reached 2' out of the red solo cup. I'll have to take a picture.

The sunflowers started very quickly and are actually like 6" tall already - wat. And the yellow peppers I planted on 4/27 finally graced us with their presence maybe on 5/7.

I also planted some old alpine strawberry seeds that I had laying around. I'm not sure if they'll grow, but it's worth a shot. They're small seeds so I think they take several weeks to start.

I'm doing my best keeping the weeds down and watering all the plants outside and inside since we havent gotten much rain for the last few days.

Also, it's mulching season around the house, so I've been mulching like a mad man. It's looking extra nice this year though!

![mulching](/img/2021/mulching.jpg)

Lastly here's a picture of Oliver doing his part by watering the radishes.

![oliver watering](/img/2021/oliver-watering.jpg)

### 5/16/2021

#### Part 1

I transplanted several of our biggest indoor-started plants outdoors on 5/13. This includes yellow peppers, beefsteak tomatoes, cherry tomatoes, marigolds, and sunflowers. Today, three days later, many of them aren't looking very great. We get a couple hot days this week; maybe that will snap them out of it. When trying to pull the marigolds out of their starter cups, a lot of roots ripped, an I didn't get the whole node of dirt.

I have to say, starting plants indoors this year was a bit of a failure, but I'm definitely learning! Our house's largest south-facing windows all have exterior walls on the left side which block the sun until maybe 11am. I think that's a big reason the plants are so weak and sick-looking. It's made me seriously contemplate building a mini seed-starting greenhouse. If I pursue this idea, I might document it in a new blog post.

#### Part 2

It's later in the day and I did a bunch of planting today! I planted another row of carrots and radishes, a short row of green beans, 3 rows of corn (half length, then I'll do more in a few weeks), and cucumbers.

My neighbor stopped over and showed me his new bees and hives. We're seeing a bunch of them already at our crab apple trees and in our yard! It'll be fun to watch them throughout the summer as they interact with the blossoms in the garden.

### 5/20/2021

We visited the store yesterday and picked up some plants. We got 2 varieties of flower, roma tomatoes, serrano peppers, rosemary and basil. I planted some flowers and the herbs in our little standing garden pictured at the beginning of this blog post. I haven't taken a picture of it yet during the day, but I will soon and add it here!

I've seen 3 potato plants pop up now, so that's half! The newest radishes have also started coming up. I think I can start picking out which little sprouts are the carrots (from the first planting), they take a while to start. I'm eagerly awaiting the corn, bean, and cucumber seeds to sprout. I imagine the beans will be coming up very soon. The last few days have had soft rains which were needed. The tomatoes are looking pretty meh outside, but the newer ones I've started inside look pretty good. Also the newly purchased romas are a good fallback plan. It's been hot in our house the last few days and I think they like that.

On 5/12 I planted some alpine strawberry seeds which I wasn't sure if they were viable, but a few have started! They're microsocopic which makes sense if you've ever seen the seeds. We'll see what happens!

### 5/21/2021

Well it's been another dark and dreary, rainy and stormy day, but I couldn't not get out in the garden! Here are some new things I planted.

Six serrano peppers:

![serrano](/img/2021/serrano-peppers.jpg)

Five roma tomatoes:

![roma](/img/2021/roma-tomatoes.jpg)

And here are some pictures of plants as they're currently growing:

![onions](/img/2021/onions.jpg)

![](/img/2021/.jpg)

![potatoes](/img/2021/potatoes.jpg)

Finally here is a couple shots of the raised garden:

![raised garden 1](/img/2021/raised-garden-1.jpg)

![raised garden 2](/img/2021/raised-garden-2.jpg)

### 6/3/2021

Wow it's been close to 2 weeks since last reporting, let me see how much I can remember. In summary, the garden is looking pretty good, I've planted more things including some potted vegetables and we had a late-May freeze over two nights which required some frost-proofing work.

Perhaps the details of what exactly I planted aren't that important. I say this because a lot has happened over the last 2 weeks, and I can hardly remember it all. I will say that I've planted the second round of sweet corn on Memorial Day, May 31st. None of them have popped up yet, but I bet they will soon! I also planted some tomatoes and peppers in massive pots up on our patio. I hope they grow well because it'll be fun to watch them up close as they get big and produce fruit. One tomato in the pot already isn't looking good, but I can replace it if needed. I've also replanted some beans and cucumbers that either froze or didn't come up.

We had some very cold nights at the end of May. On the 28th the temp got to freezing. I was about ready to just let the garden go and run to Menards to pick up new tomatoes and peppers for whatever we lost, but in the end I worked on covering them with some sheets, using bricks to hold them down and tomato cages to keep them off the plants. I also used some cardboard boxes. Here's the result:

![freezing night](/img/2021/freezing-night.jpg)

All 8 potato plants have officially popped through the ground. I'm not sure what took the last 2 so long, but they made the long journey. Our potato plants range from a half inch to 16 inches tall.

The radishes and carrots are looking good! We've pulled a couple radishes out for snacking, but they're still quarter-size or smaller. Another week or two and our early ones will be perfecto.

![radishes ellie](/img/2021/radishes-ellie.jpg)

The first crop of sweet corn has popped up, they're probably 2" tall on average.

I also want to say that Oliver has been actually legitimately helping! He uses our mini scuffle hoe and actually clears out a sigificant amount of weeds! He's very careful to not kill our garden plants too. He turned 4 in April, so I think he's doing great.

I was _mostly_ successful in getting another family picture! Only 'mostly' successful because Kaytee abstained.

![family 3](/img/2021/family-garden-3.jpg)

![family 4](/img/2021/family-garden-4.jpg)


### 6/14/2021

Well I don't have much of an update, but I took a bunch of pictures! Everything is growing really well! Oliver and I are keeping it weeded.

![june tomato](/img/2021/june-tomato.jpg)

![june corn](/img/2021/june-corn.jpg)

![june flowers](/img/2021/june-flowers.jpg)

![june onions potatoes](/img/2021/june-onion-potato.jpg)

![june peas](/img/2021/june-peas.jpg)

![june peppers](/img/2021/june-peppers.jpg)

![june beans](/img/2021/june-beans.jpg)

![june full garden](/img/2021/june-garden.jpg)


We also pulled all of the secod crop of radishes. Here's Ellie carrying one of the biggest!

![ellie radish](/img/2021/ellie-radish.jpg)

### 6/30/2021

We've got some mini tomatoes started! Sunflowers are probably 5 feet tall now! They stretch higher than any other plant. We've been pulling peas off our almost 3-4 foot pea plants. I can't see any peppers or green beans started, but they might start soon. Here are updated family pics!

![ollie peas](/img/2021/ollie-peas.jpg)

![fam 5](/img/2021/family-garden-5.jpg)

![fam 6](/img/2021/family-garden-6.jpg)

### 10/26/2021

Well, here we are, almost 4 months since my last update. I believe my absense was due in majority to the garden not meeting my hopes and expectations. Therefore I was discouraged! The tomatoes were especially lacking in number and size of individual tomatoes, and I think the cause was cause was two fold. I didn't adequately fertilize the garden this year, and secondly I planted the tomatoes in the same general location the garden. Since my last update, however, I have been taking some pictures of produce that's been harvested. I'll end this blog post with those pictures!

From some large pot planters, we got maybe a dozen green peppers like this one. They were very beautiful and delicious.

![harvest pepper](/img/2021/harvest-pepper.jpg)

Here's a nice picture of root veggies arranged in a bullseye pattern.

![harvest roots](/img/2021/harvest-root-veggies.jpg)

![harvest corn 1](/img/2021/harvest-corn-1.jpg)

![harvest corn 2](/img/2021/harvest-corn-2.jpg)

![harvest corn 3](/img/2021/harvest-corn-3.jpg)

![harvest cucumbers](/img/2021/harvest-cucumbers.jpg)

![harvest carrots beans](/img/2021/harvest-carrots-beans.jpg)

With those carrots I made this super delicious carrot cake!

![harvest carrot cake](/img/2021/harvest-carrot-cake.jpg)




