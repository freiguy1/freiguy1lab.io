layout: post.liquid

title: "Arch Linux auto mount USB storage"
published_date: 2012-12-04 17:44:42 -0500
---

Over the last week I've been working on getting my Arch Linux installation finalized. One of the latest things I've been trying to do is get USB storage devices to auto-mount when they're plugged in the computer. This is useful for flash drives, removable hard drives and SD cards (through a USB reader). I posed a question on http://reddit.com/r/archlinux revolving around this subject and what services people use for this sort of thing. One user answered with a service called [ldm](https://github.com/LemonBoy/ldm) (lightweight device mounter).

This looked attractive since I always like lightweight things. Well ldm exists in the AUR [here](https://aur.archlinux.org/packages.php?ID=60088), so I downloaded the tarball, extracted, makepkg -s, and installed using pacman. This was all well and good except I wanted to be able to start this daemon/process at boot as root. Well that shouldn't be that difficult, but I wanted to use [systemd](https://wiki.archlinux.org/index.php/Systemd), the new Arch standard for starting daemons.

After checking the ldm github source again, I noticed there's a file called ldm.service. Well this is exactly what systemd needs. After downloading that and placing it in the right spot, /etc/systemd/system/ldm.service, and configuring ldm correctly as indicated on its site, I could enable the service with the following command: sudo systemctl enable ldm. The output looked good and upon restart and plugging in a USB, it was mounted w/o me doing a thing!

One more thing working in my favorite-so-far Arch installation.


