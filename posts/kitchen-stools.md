layout: post.liquid

title: "Kitchen Stools"
published_date: 2016-03-03 14:48:12 -0500
---
Our kitchen island is lacking seating.  With some stools it'd make a great breakfast area or anytime eating area. We did have one stool but a set of four matching stools would be great.  I thought this would be a project that I could take on.  I've been able to get a ton of walnut from my dad who had some sitting around in the shed between tractors.  I thought walnut would make some beautiful stools and it matches the darker wood of our flooring.

## Design

I've acquired a couple woodworking books and one of the joints that really caught my eye was a half lap dovetail joint.  I like the look of it and it seems very strong mechanically. I decided to integrate half lap dovetails into the joint from the legs to the crossbars.  This gives every stool 8 half lap dovetails (4 crossbars x 2 ends each).

I decided the legs will connect with the seat using stub mortise and tenon.  Stub just means the tenon doesn't come all the way through to the other side of the seat. The trick with this joint is that the leg does not hit the plane of the seat perfectly perpendicularly (the legs are not *[normal](https://en.wikipedia.org/wiki/Normal_(geometry))* to the seat). This means I need to either angle the tenon or angle the mortise.  Usually an angled tenon is easier than mortise, so hopefully you can see what that angled tenon looks like in pictures further down.

Late last year, I built a prototype stool to see if my ideas made sense.  I blogged on that process [here](/posts/prototype-stool.html).

![Sketchup](https://lh3.googleusercontent.com/PaZd2rg2-3zGQ7ddMi5JXsUERCselYAbOGA4D6ChnLoMuWotOOdRZ1WwosxgRYyYN0UXXB7ExHWePSZCkTx0wjYCAhzGvYQbvP12Mq2y9CgvYCsGzzAh1hNTx4xmY2mQhRkTMZYZkw6ISw9-kp9PcsHSgYgD_iECpL6f1r_Tq54p28TtZ6GOOoaGlGIGEO19OB2oukKrTOge0ZqCcIx6OgQXjns_c1xW1yfINvATuUetbvNqREnYHKIVk2KsbFGuouQeOxRiSfaLWwbw-GgdhJXa8JiVreHB8sITq3SgWwD5wPT6rucARthAH-xqbOi2_ulUa9vjXTuHSOm1BrAhRU6Sl4E9MR_xTrDlgUeaI0GGDf3m7lzbD-GYVFY1OX6sarXUPUTEs8TkAvIVeTwDyAGxFrvt0rrpRHXitYq4cw9iiLyK1Fv8s_MbGjomo4HXQ2QrJu9rpdR612_pHd3HlKuAW-W6ZGxdsVXHHqGVMb39PlKeo9Pj28lmmLmdZlzGiHRJe7vNRAyuUXZ-XoUQGxQoS0mC-3jKTLSDdzmgPi_R4ciJps6JtpEhUdUiNlBLWftK=w824-h500-no)

## Build

All of the black walnut stock I have is 1" thick.  For the legs and seats, I wanted at least 1.5" thick so I had to glue many board faces together.  Here, I'm gluing all the legs together at once for the first stool.

![Gluing legs](https://lh3.googleusercontent.com/smx4b3burey2tAqG76B3MV-2GWmNxqcH_Tnwwe-g2l0lnGpdklag_J5Cs-jlUJv-95D1opJTQioz8KsT6vygfLcs2OStZlszxuz_eNqGc1yNttB48i5sEuzO4rZZGnl0dj4gMb6EBeMZAdEgWUONqsQqedwz83cPWAxRmJvmrN9iDnMLu3gvgKSWRv8zs4P830z9Zlye-2kjCzwVe1Rfc94QrVfgJqe2XWdhP1120Ln0RQiKi4BVPQXErrsuv3yhDNQvopTp3c0gFlPVLnUjFcvfz4eECmPIdleB0CFu86ZotH2-edZweoGY2GRNffMGsh7i1phFtuUG_58dKPE7srcKJJxZcBe_yqnfD7kOYCpxfOZ880pa15Og29CwJN4vPUT3kH8esLth0xSZxS9BzYi6uuZC0eVaNb7WABwrIBo8nnSfHGaEl2o7JJepN1C9sf6C6FftW2O2onoDh6o5Az5d2NC9-wc4PyLKeEVA63M_hLjBwIv1Oe7TZmw48zn2PA5Tm312jAa9A-dJeEj7aQ1kW4lf0KC_8hRq1FDB0Ka5ZGElngYtp2YbANQfeEBBsU3y=w968-h544-no)

I cut the legs all to the same length with parallel but angled cuts on both ends. I built a jig for arranging the angled legs so I get perfect alignment and orientation every time.  I cut the crossbars to length (a bit long) and then cut the tails out on the table saw with a tenoning jig and the tablesaw blade at an angle.  I then arranged the crossbars on the legs and drew the lines for material I have to remove for the half lap dovetail.  These all came together pretty quickly.

![](https://lh3.googleusercontent.com/nlaSiKG4HTI6jgLVrPTe-PQgD08yno6iBQ-ZPumOIGu1ghBmfzUjToGLS_uFCkrvHpjgp9UCYOtBFeWjn7LNWO7SbXTM4BhgLMfsBta8nDXFAcQhZOq-fr-bqjRX7EexLKElSDYEqaQwaq_-gwlQNJAd9xgtKYKeRZC-d7AzGvbt1rhsGcI58yaFvR1eN0GAGwTi4wrSxjFoPalgv0o9Uw9S6CcDDAWSz-i0xzwWWYx4YeURYkPkSBTgn0BsLZucfISQI77ebRzP3uIkMWdJ7ycz0DCdRJcuQNdz1M5tl7hgUTpB3jOvbcE7W7xovzp3fTsQW4CKO7WNqQ1UEHvACSZkG4D0GYCD9WQ0kWbE_LH9Lx80wGs0nwjlc9cRn1FzNmRgarRl00DSTBOlZfYne5A30KJVvV7727HSHljg6AoX-9BG1eNniOwAfzQqkP4pyAP_ZctT4v-m5JBDscR6jqaNaskSL7yCI86dS5OwF3SZy9CvI5eeaiVepc8lxTEnwFfAmm2BgQjyD6_TLk6sLJKaKfqxgNRzrjWHODYXGmlitBdmWDsKI0dnMGhQ_yCQqHFY=w306-h544-no)

Next I had to make the angled tenon.  I messed up my first try, but I didn't make an irreversible mistake.  I again used the tenon jig with the tablesaw to make all the cuts.  It went very smoothly after I got it all figured out.  After all the angled tenons were cut, it was time to chop the mortises.  This took a while, but got quicker as I learned how the wood worked.  A great way to remove a lot of material quickly was a Forstner bit.  Then I used a couple chisels to finish the mortises up.

![goal](https://lh3.googleusercontent.com/K7HPJuIvreVpDLqYaaaYAIH7ZgPe58MQSzYNoeRl5UzglG676xYT_a7UEfk-7pDHA0SmB5e2DLN_twezspm-gA0hIAH9SuRFRP96Zn2n4SZSidUwuvKAHoLpgise28T7d9L8y7W1ERL1PQUNd23MtYH_N9ZuxJ9dBiuW5fUQu-X-LGZwTHpQJ-OUaYULmTCNlGX-43wYs4JJFxC_-isHZhmsaBVXzuDKnJld6qFbSYlqdlbgPQ-PX91sZnNzkipBdT75c4b5jSU_rAoMaH2OpEyvUEiaSV_750u3LA3LLbdQp0fLrHCoe2S3ICJWqu8ein4ZF5gqFMusYrRLo-DrAHK9Qcuyhf1O2lzwkb-sXLU_Kyo6LRq5JMbfZk1yijevh6Cr43ztA89tMm7IJYolW-1rcXWP1RGUI_eMA1SOTVtV_4mvgSL5pIiyK-dBmG9S_hlxvREReZOU7uoZKVs16PhUFtFc7pdFRsPdypbCEXbZHy1dPWPw1rVPmz6g60lPog29Vy6SnSkh414UFqKaXiJEFxqjk5XKL703afeoutMhDmJKE1P8wHK64pewhwUSYIv7=w896-h544-no)

![result](https://lh3.googleusercontent.com/Q3bQ5F2iu16oiV9uNs6z-VVJ-1vx0ZhnYUTAvfKpC_TJl_oUiLbBaezwtrh2fYXMY-D-d1zFl463sZyfSqFsvM1LUgPx1byhQRa5W9IFqazcB-86UACyIlVGO8wfMDhqD35l29bCLjZAjq-UA-zf1Q9Mw2DERqJsLNAUzNUH-mdNC-6-yiP32rYNz4KnSdt0NkiZ1ehoRRsVfHFeyeoHCdESWuA6BSNmstdHACkuj1DFJkF3IH91mFbkOtcM1W7y7kLF3bR9dh0oq98ZN_d8EBaCa4HDJioQjLxyCq05pGi5DMoLAq-WfWccwbATZ_LPCOibBHTOfXgUnTFpQP58v5JJGoKlCT66yIqI7x1tQsie_rnAgI6K_9kvnSiJqbVVl507hsCmcGqDAU5svG70qCdFzcVqp6HLM5VMrq6k7zIHoqOg9m_IQlAtyfxIx34W_vPhZTGKYu9m3pM42bLYU86kovH536FQ3ls2c_mnCFjc_WxPGO9Y8KL6DZvH9DoyPuydCURdE5hdxBdxQbzu3vLXZp9cObai4wqy1kNufmTK_g1xPxiveEV8-ZEDOVqJ07n3=w408-h544-no)

For pre-finish work, I rounded the top and underside corners of the seat with a 1/4" round-over bit on my router.  I broke all the corners of the legs and crossbars with sandpaper especially where you rest your feet.

I was then able to assemble and glue. When gluing, I clamped the whole thing down to the top of my workbench.  This kept the legs aligned and after gluing the stool didn't rock at all!  This went decently with help from my wife, but I could've done a better job removing excess glue.  The finish really brought where small films of glue remained.  For a finish I used two coats of Danish Oil natural color.  It really brings the black walnut to life and is fairly hard to mess up.

![](https://lh3.googleusercontent.com/TtOXG3-1VL-A6Acf10uJJZNY_H4k141ibJrr_eP_CwEluDJi-L86kobTZRY_w75ZAh0dfR2W1QmCZtyDqu9CjJ4tyzm13MHMJ9ZvxRdIBG0zvve7xAMO9aCX1WE53RE-50YQSx9_qfzorakMGrcOt6toOUQd71vjJBPvNxWKjY55xA_1ou6AOrC6xDWXBmigZQLnAJRVRz5UJoQ7S1ZRHuk2DPvr7Qz3hbe2q8S5J-EPEGvOCRqAq28E2_ex2o2jR3ngeBMaxW0FXsFKDXnqH953ldsxdVZTuU_B_m8hzL4l0132v_XyJzH__yJbIAiBiBGOKpEwHclTrnv4_h-pzG4YDY7D5avt8xpLMst7BBbINQDg9W-a8jlRCKsh2tzFUsnyu6sajRZ8ahpNWeEO4yODJMTJ85xDIxKbwd6O1fTxWi5yDYKL0ut3cZRgPuo2c6P-7Fcoj7alEVNBOb8-4oVAjsyLU99oS7_wkAhoRGTKz2ENx6rD_baBcptdcpnSaSDexr6U_h8d_hAu7KX9akrFmW46zmXL66TCfPvKxBByuMsOtxBS3x01rwda7Yo11PUb=w306-h544-no)

![](https://lh3.googleusercontent.com/FMJEFMkndpVL4UhlLhSOIuAcKrQ3LTOK0y9ghteOwSMatC4QpBSw2GvvzsQkOHxXNCv8ZL6Qce73rZc1q1kycoNi6VM7YkWtBA8nu_DbZuUpsarsLtmLtn1PsnIIAeheVxfXBxNuTkMjwLbNNfvt7GiXhKLgPqf341W0rCg4dq6RNGg4zLqCGpNY0QcvBGxz2v-ID968T0ZOtuUTy5nO94L1BzhNw9P8xux-ckDV2MU5sWO7VxRzo_VF9QYTCq-Ny09wOfKKdvprqK1hjYvGdSC-2IVgIHkVTZuio6hEAZaECfmdqh3UNXJVWpptZ8lRVXen2E7omTlyIorQMW_oze_-tRzUsp3mkosIGZN7BO_bCbqnGNw8fHw8OFjp55aBgSdmchGgSTZJmV2rZGLrY5NG4O9SQdcYRnGydGFnr5l-gXdgDBJM2me_t2mnadnv8a_qFsXCNJscZ4NoIZ7SyY8NlWbhXPtxK8XpnmZNTy5y-Z-iXe7BqYLun2LWj8VvRntULqjXIwLzSbX97e7bc5HnBTadER_Ib6jvLviINwfyIW5UvZ5bKrBS-CLzrL-5etqP=w306-h544-no)

![Finished 1](https://lh3.googleusercontent.com/MLxzW2j9S0NErk-mQcS-QcZ1PbpEATmJ5gkG2gxxgrnPiw0KNebfiuGr--adoFpeYxB-PI4lEiZlDkXDyLXmAbJV5j113t_7OqX0nMYlw-LLJf2i9hltzHCa60bU6Fj7W77VjLjvEWjTI0UWU3F0bWQhK2XbP9y-td6Pn-LEHMQqY1s59F1hnJcGTXWN04xeaId5j-ab1bpWTT3U39zLd19OsKEilKR2S-evj8XKbzJW5MSqoQK6K_dQWnZ4C2KQ3pVxD-zSLfDRKTbOJYHhnAPe_A4rZrt3d7lS4or2MOFjRd1LlrhV0hlZZxEA7zXh8bTwtHnrIkFyqZtBZ4LZ18AgEYRIVglHglKHWlUucUxMMOY3HFz016x1DXK1MQJp4XUFY_cTDdCxWvS2na3IvQ5q3F1i1xhfECDiKnBAhovVtAYO3iI3J7F609ypr57c9X0mgjlbCpfCZx708jCH9JdjX3s7pjeI3RXUFgTv0hgzGTZsulDvnrNNsBDiPfdLYrlCOqFUzY7xHdgDsCCn56xQpFr8IiLQ8pe_-BEW_YHbyj6vumsByPAVt8xNswpZa45m=w363-h544-no)

At the time of this writing, I've only built one.  I hope to add more progress pictures when working on the others.
