layout: post.liquid

title: "Scala!"
published_date: 2012-07-30 21:30:39 -0500
---

A coworker recently introduced me to a new programming language. My usage of the word *introduced* is a little off. He briefly mentioned the word "Scala" in passing saying that he was starting to move his personal Java applications to it. I take this guy as a pretty smart guy so I decided to look into it. Little did he know that this would become my obsession for the last 3 weeks and for however long.

From [Wikipedia](http://en.wikipedia.org/wiki/Scala_(programming_language)): Scala is

> a multi-paradigm programming language designed as a "better Java" building on top of the Java virtual machine (JVM) and maintaining strong interoperability with Java, while at the same time integrating functional programming along with Java's object-oriented programming model, cleaning up what are often considered to have been poor design decisions in Java (e.g. type erasure, checked exceptions and the non-unified type system) and adding a number of other features designed to allow cleaner, more concise and more expressive code to be written

In short, Scala's a hybrid functional *and* OO language that runs on the java virtual machine. It was made by [Martin Odersky](http://en.wikipedia.org/wiki/Martin_Odersky) who was a vital contributor to Java generics and the javac compiler. I've most recently bought a book written by him and a couple other guys called [Programming in Scala (2nd Edition)](http://www.artima.com/shop/programming_in_scala_2ed). Using this book and other resources, I'm on my way to learning this new(ish), cool/hip language!

Trying out my new syntax highlighting plugin on wordpress:

```language-scala
object Main { def main(args: Array[String]) { println("Hello world!") } }
```


