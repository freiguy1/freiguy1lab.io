layout: post.liquid

title: "New house... new workshop"
published_date: 2015-09-10 15:49:45 -0500
---
Kaytee and I just moved into our new house.  It's the first house/property that either of us have owned, and  describing that process probably deserves a post by itself.  However I'm not sure I'd love writing that though as it's been a multi-month process, and I'm sure I'd leave things out.

One of the huge reasons for us (probably me especially) falling in love with this house was the detached garage/workshop.  In addition to the house having a two-car attached garage, there is a 30'x53' detached garage with two garage doors.  A single-car section of it (30'x13') was added on more recently, but the original garage is all insulated.  So it has an insulated garage/workshop which is 30' deep by 40' long with an electric garage door and you guessed it, a furnace.  

This amazing space is going to easily facilitate the new hobby I've acquired: woodworking. Before even closing on the house, I bought and paid for a table saw.  After a lot of research and note taking, I decided on the [SawStop Contractor's table saw with the 36" T-Glide fence system](http://www.sawstop.com/table-saws/by-model/contractor-saw#overview).  After the research and a few discussions with Kaytee, we were pretty concerned about my safety and peace of mind in the shop.  The amazing SawStop technology stops the blade immediately when it comes in contact with any material that conducts electricity.  Being a programmer by trade, I tend to use my fingers a lot for work.  If I were to lose one or more of them, it would be a much more significant loss than the extra few hundred for a SawStop table saw. As of writing this post, the saw is supposed to be getting to the machine store I bought it from tomorrow, and from there it will get shipped to my place.

Until then, I've been hard at work making this new workshop into my dream shop.  There were two very large work benches along two of the walls which I'll be leaving.  One is currently holding a chop saw, and the other will eventually hold other bench-top machinery (oscillating spindle sander, disk/belt sander, a smaller drill press, etc.).  Underneath them is very large empty storage areas.  They're almost too large for hand tools.  Perhaps a router in its case or a shop vac could fill the space appropriately in one of the sections, but I have neither of them.

On the workbench that's holding my miter saw (which was free as it was inherited from my grandpa who passed away several years ago), I've built extensions wings to both of its sides.  Also, I attached the saw to the bench with lag screws. The wings are very long and essentially take up the whole workbench, so at some point I might end up re-making these wings.  I hope to get a sliding compound miter in the future, and then I can rebuild the whole thing.

![](https://lh3.googleusercontent.com/maAw15VL_pA8kRQDOJxaYrfJCqkRsJc0sQCFEKIkt9FEipkmDt2scJTuE2yiXIfIbuXAJsL-ReEEO9t_4udY4EUNz6X47QYEE8-a7jfjcSxDafcC_cpmZjKrlQIIV_mKG351meaDPNj-EmMWqF5evZPVK6SG_5MX_kxpgbqEVOpphFHMS01HUgzAMmkahrge4p0PtBrnBhHeYvwQg_2sol8AttFtV2b_9m3VuBA3khEcr26rIp_gwILejOq96NBDmyMfaYT7SH8yD61obvOcP1PD-SbGeH-FXeyblbQd1by7QW7aUogvMbfS_Mt1_UK8_mKIQX_jnys1Z7AbzxaPj5VH-WqzNjOppvIuk3MT574d3tAG91F4Z7c-MU-3QOCmaTpyT11dezi_toBCtrt9BhHsyFxqeX-7D9ECHEzH0WqFYhfoDvUuZTf6TwlW4qRv23-2073ql2ucMPsmfJUEyuqHUxuRlt6-fIejw4s1Y_WNU7IG4934I4jhU-oXhYANk6udY2yd5-TCPfl5TjTbgUs=w531-h944-no)

The previous owner had 4 steel shelves and a 4'x2' sheet of pegboard on the walls, and so far I've removed two ofthe shelves and the pegboard in favor of a French cleat system above one of the work benches. From [Wikipedia](https://en.wikipedia.org/wiki/French_cleat):

 > A French cleat is way of securing a cabinet to a wall. It is a molding with a 30-45 degree slope used to hang cabinets or other objects.

 > French cleats can be used in pairs, or with a cleat mounted to the wall and a matching edge cut into the object to be hung.

Here is an image of the pegboard above the workbench:

![photo](https://lh3.googleusercontent.com/nJUckLOqIJcP9vQhfHso9Mh9phCLhWweb6WCyW1ENWDPOW1inID8OzSXlcIWsFlXSNYPXvCkjk712txwvCMkNLUibcO2jjAnT05-DlW71iq7eKTC0lO9BckcEsFPtrE5EUBJGSLkR4DV9BiNPUW2hmlxzIJsGW6I-qxP6yYUmTCXKZY0Z9EoBkdODHbOSed4zX66yREpW9I-qNdoWmxlfSfLYI8zk0I_SisY9z3qMVmM09LCUx_fnr2xtiTG1xaH6tDMbVERZdlPl0djzvNJvpxSxsjrzS19UpGe48ER24u3XJwMeY1kBBpEwcrK4NwogZSQkHINV6M_ZXIV3wW05WuEDTzi1C4kKbJyKJVdoTQ323chz5mmXxQKqyts740ZvX3x5oJAwmLtvG8JmqqvofcFclDEW1vozFaqZe5zQKuiJe_HDDzhtNRyvNKCxvG1ah0QEKfrUDPpi-QPlvNwGbD5z6-R-px5lCo5a1Q7tFduw9jZQtMMabPKn8pH4T2D4NohT3ApZLQofNNHpztZ088=w782-h900-no)

Here is an image with all the wall cleats installed where the pegboard and steel shelving used to be.

![](https://lh3.googleusercontent.com/DG7i2qhDCgy9OSgaR2rjyz2X8cXoyD-tLDMSlR0xB5VssJOveyRPStlr-KsznURL5oxSSoxUnYYSw3B3qVH_LmPJhEEpKE-CBqPpQRtXtHdPXdjhHjRyEPq9E9qHjtUV_Bw7N6-hzMXvzAb2urMc7QhuLptSbiKofxo3GFbAwkh86UiIjl-saUsdfagfztdvWNKFCFjSri3dezQxnr18UB9ff-bEiQTXwxrftjs1cep2fGm2xw8U-pe5h3YhSoM_LRxloAe0SF8yu-e-8FIhfEknCwNcDqgp4fry7DbnCi5KBwxHzX3HIfB59dcHt98ZcmHaO7j1NPpTX5ppbRiB2MfwIkwnQi2IU1tmb7_yPbaz4cHSyvJXC_iqsI2u5RjbwKpNFPE-_sLRWEo7DD5p8DJSnP-f1wVYaQUMZjAsQxB3kUNuDDXJXUrbkVAPvLq1_4Mtlgy9xys7qPKJeGK4_u0WvPBpMlp4U3OdCDlLZYq_WiRx3P5WahwTn9NG2lpPUEMQq19Eteq8IN3IlOQfJ2k=w507-h900-no)

Before building this whole wall of cleats, I made one specifically for my clamps.  For not having much of a shop yet, I have been able to acquire a decent amount of clamps.

![](https://lh3.googleusercontent.com/GQ8DZl1y4V3TAERTk-5gdHvP6DEA22Jb0JEi7cs79SRcTdeXOUNRQnn6aXVPr2NQIvYrl0wE7UggrEEw6MsvuLzphvb4tt113QfnR4BTknvkt4JLPu6-jXgRod14eoqLrVzi6ILvBsXUUR6jgmmx0W7FRimxv4Q_H9R6pGGCnl0Dt67XTSlsiZ2dcdOSd6tJfLkwKOF2jbiV-gdWLvEo4VsqvGy7YuX4mW8qSCxHN-D1lQqYUYHSH7v4X2eJ_kapZJHPkX9k77Y062w8qtu_FLY_hIdl7vXtPTT8N_OujRIaZYz-KwzTZJRd6_jj8yTwt41qlNi4qRtdrnf412adlTV3AuoGcKawdw2sTWY69GIvLqVPlmm4VfflozxjHa3RBkgiUYw1-D0jI5BMcaAQZJ47rqo3mbVscSEuBE8iGVvRDlikCJFfYNXnl91sstpgSC0FPpVEaKm0wHbC9NA-Ud-eJucECvC5Zhs5pZOtLz2WxuqEC6NJ1SrtI5fXVGtG08pMopBn4sBO1zy9yVpO9z8=w507-h900-no)

Here are some other attachments I've made.

![](https://lh3.googleusercontent.com/A1zwCF9V5EvdIQ5CxU5GP6yD3gK81zXWu8JNm14LOgN4riwApmuYnFOKcVIYndzscUN3P9jy-B29YQHalx4JPZJeHqgZhHwwOJb07xnL8y91sEWeB3RMiH-U5BjdQvtBWrQldA7zybuKOUzgFP3j4p9_O_zLJUxWmhbbHkCiWoXnKgOrAqonzAz_adg0_FoQ30ScZDVAmr7qRnCzkU-Vc_1VGBSFcLvZ1z3JPREaF3oqfh7wAl_-_U-KR2zYtYw89PPDw5n7Da-kHlBod26uCucA4u-LsQtQwLSMYTDiRDTwIYbwRPScoCUGAem2Wa8yf4EPV6exPHqcexPKHkwiY0FQO5xzIW7jgVyx0n7cnew-E8EhX5obcEAWYE03-dBQ0A4-rD8Wq5R9WPII9ChBt7K91uI_pdZ7Tb6sEsg9vyYPwx_4Hvu7ye-BMHtvusXzImK9MvyCIIwJyWIzHXB7RFs814NFJEpQ-P1DR1bcyFeXJAUQ8WqLuzHv06Hg1PPSoHAizTsA9bKAxtX72xmoJVg=w507-h900-no)

![](https://lh3.googleusercontent.com/EpupjA3QFSgJwodlPADeUGLSATVQfrBT9Ip70AbHod9uNMj8epE7niCl-o2bEJMCQuPDfTddCV3XDBqJRBwENOOo1vblEmPB5E9Sfb9hljFgtBedtV_4z6pxL62tKdP9SRUrOeRNTWNcIWctEBb9yePYikDypMrSfHV7PKPb1X_2rZARNKVsqjvqgWsuWZFbt1oxPo36eaPPUpORmBGmj-6IxNguLvT5NjB_dZRsoc6-zr7sdRKcXka__hjZk3ZNlE0RfRMgmgSnUbujyUmzZiOS4SdcykDRnJZ5gUSrpyN70xb-bTzU4XvqpRV3TeaoNUI3m043uN32zrNZRxG_CsgCDlei1v_hhrc3SmRX5mB_ZnJ-ix9Zn3pRuG3cttLakTmDNyutp2VCFOhIYGUwHmkU4ncFIMfI0STUW7RKOErmX8LQMs8jA9unH9EJRBWJaOof6m2RqdnXYhYVINe2xXZ32ig7S9XX49_PEOddL8oCgeemuWAhKxEWveCLId3LzjoFypFN1qf7g8nyNig5jwQ=w954-h537-no)

![](https://lh3.googleusercontent.com/3SvZ7X8mW5K9GLhcfYHzOK5L0zcrn9Opnv2m6ubAjU2MiuHXUwVok7rJ_HFNw3xQj_YVy6x_NK1PrzucpKL_iNM8IxP6x73o6StOxd_pgXhoLaC2bVujac588cY3y63MHmMFltTxiRSLnEo9IKCieQrfV6nn1qDqp2eBY9ZY0Y6KFDOoFsqSnR8k_jXjb7YHTeBlVHmA3ccfTM8DlyN0SqXfyJCW4QBONYovbAxGcAUHK06fG7ciR6cmj7BBElqAObnCmYU1-uA3YinS3Jm62MCjnOPW5hOpE7Je8cCc0RRHNBnvmUB_TUK2CwgKSR4R3DjreHAY1PQ88esVRMEAGv0PbSo5D1hh12M5QigiSzWhCDS8R0KMIIoeoRNPKH3tzeKE039RT4mfVyUKo3jk1swCAe0nqLEgn_BdGUNrVloTDCOlPqm_rWUwHhORnmVCrEbL9t94-XCag-wFv6BS7Uo8zKapFQZTSNbwOCZgFOL5mS4dfwv8ESWgj8R6ShqLGN4H21sSVCKe4bxZOMPdxgo=w507-h900-no)

I'll be working on custom gear holding attachments for a while, I think.  I may also hang cupboards from the top cleat and put less-used items in there.  In the near future a large estate sale is happening with a lot of woodworking items: 14" Grizzly bandsaw, 6"x47" Grizzly jointer, 16.5" Delta floor drill press, Grizzly dust collection system, and more.  I'm setting my price limits for these items and will hopefully be able to pick one or two up.  I'd really love to have a bandsaw or a jointer in addition to my table saw.  A dust collection system will also be a necessity eventually, but I can suck it up (hehe) with a shop vac and dust mask for now.

Kaytee surprised me last night with coming into the shop and doing some video recording. Perhaps I'll upload that and embed it here if I get the time.

<iframe width="560" height="315" src="https://www.youtube.com/embed/FY3fD8gwR3c" frameborder="0" allowfullscreen></iframe>
