layout: post.liquid

title: "Dice Rolling Application"
published_date: 2015-07-28 21:34:59 -0500
---
A few months ago, I wrote an end user application using [rust](http://rustlang.org) which has a GUI (a console GUI!).  The application does what many others have done before it, but it was a good learning process, and it ended up extremely fast and fairly easy to use.  It simulates rolling die (randomly generating numbers) in a useful way for those playing table top games.  I had a few goals at the beginning:

- The GUI will have a section for entering in how many of each die to roll
- It will be able to add a modifier to the end result
- The GUI will have a section for saved rolls
- The GUI will have a section for a history of rolls
- The user will be able to easily navigate and use the application with the keyboard
- The GUI will respond to changes in size of the console it's running in

Here is an image of the finished product:

![](https://lh3.googleusercontent.com/LcPVPLZsjTIRg_0MzQrvefT7E9CEyaJqIjHK5cEaTZo=w705-h376-no)

For the console ui library, I'm using [rustbox](https://github.com/gchp/rustbox).  It worked very well with the exception of [not being able to handle the shift+tab key press](https://github.com/gchp/rustbox/issues/25).  I was hoping to use this key combination to navigate selected inputs in reverse - similar to how it's used in other GUI applications.

For source, instructions, and more information, check out the [github repository](https://github.com/freiguy1/rust-roller) for it.
