---
layout: post.liquid

title: "Workbench Build"
published_date: 2015-11-25 22:30:28 -0500
---
For fine woodworking, and especially working with hand tools, a heavy,  flat workbench with abundant clamping facilities is crucial.  All my projects until now have been built on a flimsy table saw outfeed table. If I'm chopping with a chisel or sawing with a hand saw, the table surface would flex and bounce.  Any time I need to hold a piece of material still while sawing, I was using metal clamps - sometimes rigging up multiple clamps when the material needed to be vertical. So given the play of using multiple clamps plus a foundation which isn't even rigid, it leads to vibrating workpieces, rotating under clamps, and chisels which don't chop easily.

There are several fancy workbenches out there.  I tried getting some inspiration from the [woodworking](http://woodworking.reddit.com) subreddit by searching for ['workbench'](https://www.reddit.com/r/woodworking/search?q=workbench&sort=top&restrict_sr=on&t=all) and filtering it to top posts.  I wanted to restrict myself to using only construction lumber, which will mainly help keep my costs down. Since it is made out of a softwood, it will be easily dented, scratched, and marred. I'm fine with this as I'm not creating a beautiful piece of furniture, but instead a functional workbench which is meant to get used and abused for a reasonable price.

### Design

I designed the workbench in Sketchup; here are some details about it.

- 20 six foot 2x4s will be glued face to face for the workbench top.
    - This will make the workbench top come out to about 3" thick
- The front of the workbench will have a 1.5" thick piece of black walnut
    - Giving the workbench a harder front edge for more abuse
    - It will also act as the back jaws to my front vise one one side
    - I got this black walnut free from my dad, so it didn't add to the cost
- Each leg will be made from two face jointed construction grade 2x6s
    - One 2x6 will be 1" longer on each leg and act as a 1" tenon into the top
- 2x6s will be used as crossbars between the legs near the bottom
    - These are attached in half lap joints
    - The half laps are strengthened with two dowels driven through each joint
- The workbench will be about 31.5" tall, 30" deep and 6' 2"  long
- The wood ended up costing me about $63

![](https://lh3.googleusercontent.com/Q_cVHEFjSS3fhfT-Px00vjWNzX134uNLRymzebikjmNzeC2A7L1QyE6U7rpls5tKAKmRedPNHZr2xrwhHUodJ0MuZSpkZL-VvSTGvoZlyqE5a3Nc9QtmkXvdF5tJmelHpOpAFllTT1O2D05nwAmSxnTcWMNeGu5RirL-dyG9OJU0Lx_3rwU8ZdqESZwye1tSP0_C7JUemWoT1FpWKge3VKozVPIRiAKaSFpkez3zn_O5hWAn9MfGaAcqjuZBBQ_2IdRQc-uZho6GTXN4EmO-o-dMQrQAYVww3Yo0E4XE-XKed7UeDlGNaVZmKMnsZCMKKNFYlrrEew3Q-tyh9DwZBreGeKCBb7Zq4dbf0eOeSBsFtFyGHciwaoi3VcTlpohyWrmDt2o9rvcw9QAqDQ0-H4Rjrv-CPlnsEw9ssxVr-xk5jfdkpWURcDOVnrtqDUZgu7jopn02B3xCClAfMifCRQ-xjLwiThczuiPeJEGExpsoFIQkYCMNZuGS_MBfvnU0HxTA4hUlFuCIFa04WJ8oS7oqd4hFeBRH8FNPZM4RqBg=w706-h653-no)

### Process

#### Base

I started by building the base. I did a poor job gluing the leg faces together as these boards were warped more than I could tell.  I was conservative when using clamps so I could glue several at once. This produced gaps which are pretty easily seen, unfortunately. I also didn't joint or plane the 2x6s which would've reduced the warps in the boards some. I decided to live with the gaps, putting them in inconspicuous spots.  I cut the half laps for the legs on the table saw with the cross cutting sled. I used stop blocks to get consistent half lap widths and thus creating close joints.  At this point the base itself is already pretty heavy, I bet it weighed 40 lbs.

In addition to the half laps, in each joint I drilled and glued 1/2" dowels.  I think this combination created a very strong joint.

![](https://lh3.googleusercontent.com/ghRWc5eeatg1OtfpYB_DNedsz3ROILVYwUetBDeGLrMQMJ-Ht-vlL6Auf9CRvM-yn9D1xzxLFBbq34frc6Oc0hWGBXFNXBi2ORHsX-ZTEpavExcePQNqrSQZFRtXUJg9AZRVXif01s4aCYjZ699pJh0EtTpnHygeTF0z9ALh-kF8KyPSLTrSsV0iNVx3dNooNW4bxjAf9c6lxW8mocgGw0sRkJTTKMVvCdNvmW8jBehzVMod4OEKVA4MYHY-rpt7PKI9HFTpORHfEN3oqhGM86-qd2Cgq2I7UmMoV44RAFMsoXOdd53K4PPh51z6RWIrUoQug5Ck1_mPg02otZkXWfGQwprJ1VqoGmazdLT9Zes38hrte5hHiS8QSXGbs_0RPU7dulW_qnp4A4UfNegGVVeZCvx_fELoDKLUP5AU-n-AvjBsZ8j2SLzRkHdCH_CkSTXJb-4EcEIQcXGRW5FlsiDs3UfLc1u-QqWs2wC5HSRgCcPfQ9sqHZifMNYLo8gZ1j92-UnvB1znHuegCVHUxEgS0Jrovj8738TvhRfaaTY=w368-h653-no)

#### Top

Then I started on the top.  I bought 2x4s in several trips to the local big box store.  I tried to find only 2x4s which were straight and had at least 6' of clear wood on one edge.  This edge will be used as the top surface of the workbench.

Here was my strategy constructing it.  I wanted to be able to use my Ridgid planer to flatten the top, but that can only accept boards up to 13" wide.  So I decided to make the top in 3 sections - seven 2x4s per section. My wife graciously helped me with these glue jobs. We used all my clamps for each gluing.  We used my pipe clamps for actually clamping the 2x4s together, and we used my other clamps for placing flat boards on the top and bottom attempting to glue them together as straightly as possible.

When the glue is dry, it's time I tap into my elbow grease jar.  I attempted to flatten the bottom of each section with a hand plane.  The hand plane I used is old one which I attempted to refurbish a few months back.  It's about 14" long, I'm not sure on the brand or number. I used twist sticks, the straight edge of the hand plane, and a 4' level as my straightness gauges. They are a quite a bit of work, but with one flat face, I can feel better about planing the other side.

![](https://lh3.googleusercontent.com/-hZPZtqed_EttW-5D6mEQH6ZUSyp1vtoltF84t_4eZk0tiw4Wsxg7qJIDnSgHjjrlbv4YHGw_U_EabTsy85_jp9ZKCuHwTy9AkpIvvUZP7WYWVg3sdl-_XBFCbKlpS-1pAM-5PJeCOjOgFtUMmSdZG8QvsnEkorCroepbyFjGuyWNzqS_z9c3Kcbs-DNjU7xSqApTp-i-Uny4CTv9ky-fy_9o2-t4muC2CXxfxXfuRFU8jGcuioMG3wDEYyePcjIooPeMt5bcQCywRE2drlTMAE1fHm5vhhVVa7rPu2uJOihPwkt0bLPTBdurcGb511D-DXumi1GWkbEquU5ffxD_hEBrdlYTgSXvWa5jYnnfBf9XGlZ8A2LPxfkMKrv6dc_PoCWXdFAEpVAi7tE6Ja2MYCtbzFk9SCDRtK5w5R3rZEvy3nNjlSP3jUu_-O1M8XyrJAoTA6JYZGixbkGyAExhg9e7oQElG10wVfA3YbZHEK2bUwc_fwQgmV-7mlOwnJ2rurYXhJg18jYRKgz_VChrFkzj906W78K8q5hMctY4wo=w368-h653-no)

![](https://lh3.googleusercontent.com/c1o-Kf5FHVfT-3RxyHesqT43EGwG7435XB_PsO2IguWG5-iXxlxrGVORM9s6Z5GufghUW8Dwl4MzY1WCrGKQkeRA3RQ71AIw8gjKevDcWtqVaOv7DAtWOReefNOYO6GlU7bZaFPrPIt_L1pDlCwLfocIMzvDSEJevbWPuw0ebjT_TzNXbykqWKhociZjpLXxHxZj5sDVK2wjN7r3UVCL0CXkz58j3wLZK8DiYJpLB25ytcoxC28Iur5kYd5kbJIWboTWkBaGs46bTAzusUiw18WmbPSCicSpuyYU6jXTlomnc0ZbdpL1fISY0LCMFkCbmqygZuzLzXuQhVTglxXRi3La85K7WjjeWom3PWzCDfLSGEh4H5-R_u05wUok_Du6fpv9pm9Tk6SgODuEw29wh5qw_KrMVlEKYumHQA9Q_ymKfOA59FH5FZEHZbEZdo273DuWobF2jt1BN6l53whHQ3vZy2yV2BMHjCPhzvQpUGy5z4bfahv3I5hgn48JYVQEAwSPtcIZIaxhsK8rDMNj5ZW6y-sR43P27WzGuX6-0qc=w368-h653-no)

![](https://lh3.googleusercontent.com/RKcpQmak1we7MSHmpgHTQFaEUD8pFYHLSjCMKwC3-3K6F7X_1WoGuj0nLg_RrMBFjqXRHKbf7zinWtTSoo8q-amFAqAZZ8WdTJ1RLbes0zebVrhCVgU7mjLSomv4bSDq9KeyUYpKAnibQTK0oXHiWfKU_c6P1bnd1-VpNu9fq0ogOBPC83EMd08CFe4SI591aNyf2TmxTltfvAniY2MT7IOCxmw_KjSyzGYj49QzvNKbrBgDR3mgw1OOi0hJuEUG4pzbYIihth_Iy8JrktlCn7tUxgZH0GtoX3Q01g5EbrA1X3UXE9zm3M5ulgMKX_a6uWCKYjMuVce3iM6gIdhDjR5LXCDnbu1tQYrJK5oGvy0gEWtHURSGdkaEAwBR2g0_wFn3vBUa15AR-zbGknboIiqdewJYK909FLw8RX75HvTTL8JGuwPDd9RNKNY4OCFlbo7t0xFmiT9bftq0MXUHRhNfpCETQiCdkY5uaB0sNgHNTmwYTn3ON5dEueUcPBYsY-YvWKQSmxVl0QmMpC4arLrmiD9yQjrdcbewLU4PTfo=w368-h653-no)

I planed the unflattened side of each section several times to produce a flat and parallel side (which will be the top of the workbench) with the planer. This took 6-8 passes since I didn't cut much at a time due to the width of the material in the planer. These next pictures show after 2 passes and then after the last pass.

![](https://lh3.googleusercontent.com/De-HKc4-l6Eita59WENsTyaaRLmY48kTW87OSkGrWScl9aaXTcEhfeFrcrzYeJXK4JZpykNj0ddL2WxY_n7KMxEAgI1jpm2TKhWpwvRdUBWKKsAhqOVfL5xnBmaA9JHG8CWzpSp9-3wQ7dQLqO54BGk38ZOYqf2cx_Ds0hsAzj5ZgmJwQpgc9SJ6XDn2VxIsr1HSKgWTh8sADzzU67gPoGpKltgc5EcdGOgsmcCbdjuDBKt5gvoFp4woZLV-bNDSfdAFVKuB-oc0uHDYVaVTMpmmrIMyJJZkyNHZlgNsCVubdDK-pvpZlWWNPHVm6BQDJ7yKbI1a7ReWsKbGuaZSMdDJMKnlRtjYafNqfMJH0ahl7gimt0Rhr3s8saY0xF1yzBf_qmJJBWZkNAEgSEfLAuVjdaHVm2Sw4HYIpXyzFHkXMKb1y41CJObe0LzkyQuPqGAi1iQJsW0Q_mh7_xVTffz1yXL9KxniIlhk81MiYFV7eWgbFiNF21GlWoygLrIwaASl0SyJcbCf55b9wKs2rZIwvKqmSFta0fLoXdeEC3o=w1600-h900-no)

![](https://lh3.googleusercontent.com/UgCYaBVWgrvGKlO8_8NdELFYhPNRllqG1hy5ikVQ3h31tKBWGsJjiwJt_GdYIqy2XvQ3y26sk8CND7JPHl1n94zgUYup4OjcxiBpG5a037T3fogIHSYauLPT41H_yLBgEndGKxw6MCwVGvkBwfw4toVBH5hDa2ct-Noj02H6dYUEDazNTmNROF_rdt7g3bOe24zUs-7lhmCaHy2HE_cFLAFMAjWrQxRPKMI7vDKAKYDgicroo5zq5desH5tVm-pU8Fc3mFaRRbzSeymtrxHg9uNis-ZZ1nAqaRBMO-ETz8X8ulh4FjP2qjseH9ceUUthEAcP5IVDvF3LnhYFMA8E2pMJJDsm6N-MyfAo2_csirdWyUwa4REQFV0fPJ1UA_ZYUnO0VoWh0L8zCqv0zalL852PnJ-NGadk0aQabg6E-plNLQw9I2hApR5P2X8HKU3OpQ4bc_E_GhG1LxSaOOEDuDX5I29DFmxoe6oZ7M6Fz1UX58ffdi1L_5YU0VFhFfJviOqiFRPtXeGDi2YKyQNNOlK1d4pBPoOYNi8cru2t7s4=w896-h1592-no)

I then took these heavy top-sections and decided their order to make the top.  After I had this decided, I could run the joining edges through the jointer.  This took a decent amount of strength since my jointer only supports 24" on each side of the blade.  I had to hold the rest of the material which extends out from the wings of the jointer to start and finish the cut.  This went without any muscles being pulled and closed the gaps nicely.

The next step was to glue these three sections together. I don't have any pictures of this unfortunately, but I just used 7 pipe clamps, and 4 bar clamps for the main clamping. Then to make sure my boards were lined up correctly, I used 4 small bar clamps on the ends to line the edges up. The next step is routing out a seemingly unending amount of material for the leg mortises and vice.

My legs have a 1.5"x5.5" 1" deep tenon on the top of each (basically a 2x6). I used a router to take out that material then a chisel to create precise corners.  I also don't have pictures of this specifically, but you can see these mortises on the corners of some pictures below.  After this was complete, I could cut the top to length.  For this, I used a circular saw with a straight edge fence, but I had to make many passes on both sides since the material was so thick.

### Hardware

I wanted a nice large front vise, because in high school, when I had access to a proper workbench, I almost exclusively used one for all my clamping needs. One thing I hadn't had in high school was a quick release vise. This mechanism allows you to adjust the gap in the vise jaws very quickly instead of screwing the handle all the way out or in.  I decided this would be a good investment as it would allow me to move from  working on the edge of a workpiece to the face in a matter of a couple seconds.  I was able to find a [large front vise from Lee Valley Tools for $135](http://www.leevalley.com/us/Wood/page.aspx?p=54873&cat=1,41659,41661&ap=1).  This front vise will allow for 20" wide jaws giving me *plenty* of clamping capacity.

I think I'm going to create [a blog post specifically for this vise and the installation](/posts/york-front-vise-review-and-installation.html). After all my googling, I couldn't find any reviews on it, and I believe it's good enough to have *some* internet presence with at least one blog post.  I'll link that here when that post is complete.  Here are a couple pics of the process.

![](https://lh3.googleusercontent.com/VXwiqTGQ6vZ1RXq0mQOoPPY_1ZYyKDqWV_yEsPzhnTpFGoNewLGoycjjsHrRqRRD8CAwn37zNEEB-42Jy3XLNmVUOSqTPM-I952Rs1FFxK0vTQPe8G_A7QMoyGkt2-jl6PQNwpxH3PyqsOOzpi5qDJIcrSfHt6j8hZsJFJxqYHOyKIxxZDwmENFJeTNIewbLSwQnfzOduHIe6uzL5xOR2Dt8KN4qk9StE6BPgTJLodgqXlGWhFztgNLMwac7gxq4w2B5yXuD8Y6SCP2K5wbg39wslzKlwCvmQvdspQ4JaBCSpImVDAogxqOL-csp-MJFjysn65azP4JHWh0q6sfuNBtC_WFiDrh9Jf4Nv_Y840ioh0wuD_SDJIn2wqbl5BUE88b3YoBPZk_gFnDitRViux-MLccfkg93xP1sCKdg_6qKojzdWGrg9A3c1dl5-ncRCdvKAT-bVPI2qQNM8kpn4rbUZKNQ8nl0oj5bMVfKmJbjjFH2IpvDeRxnmhhyoF4Q2-r-r3aYQH8ODnL88jBlIgNvUICRzq3Dr5m9fEcU7hM=w896-h1592-no)

![](https://lh3.googleusercontent.com/nsWjjqFmSiumEl2SrUJxy5z-lGKQskQZKkaL6lZj8AOwYSp2ZPhjj2tSzii2KsvNL_edwS7PEyf32l2D5ZrvsCSDeGemk6fbeKv-w8NUYsZEM7i6cMAsYjdrEvLoHimHGWAtecGLSxt4mPDRm506SIH-OOzYGQDql-tzOFh2kdsAzxGEQWRhqDCoRl1GHdyJfAL69enV2p-rUeqdRqFSgNv__7XG_vRK3zQ60a1n98ZR3RP9cptNy0GDjGog4oLI8PYfr7o0W5ucRS9K23QxV90C-lc-NUKi8hIOBcRFXnSBKgI6SlYzKM0e_ngb-cfuM9erpzU7fsKafu8liXCm62qucTDBT4kVijQzxwn46TzXxwARqWMsx1tJgOy-piNbujEA_LPHq2a6kDfBqfNEerQolly5I7FiDeS8ivxw9R-eQsPMYp6Fq0E5VxVQv3mZCDoh0noyiRACoh3YWqWZQgn0MtIIGEy55rmb-KkJSgK930LQ8eZECspVCY3r28Lq48qgAW1aIiqZfK-xbf5t0iXphhYhO_TlrdYwvA2_0wg=w1600-h900-no)

![](https://lh3.googleusercontent.com/s4PNW7uYjZGOr5cmwwFwmpP5cqYRbNxGg7xATvvcKVbVz1G1OJTgs9cdxrNY8ldbEWD6hNCnjAupr8kjHK3vOYJuAJTD7YouNA-Njv7iqHkmZDFvFXbvKxka1C5YQVbM8uJWQRQHbZIiqB2GucxPUt9GR9D5YGA_0A9UYwkpbuO_8hyp0yjCgo9GSqAWzBmL41rByQCO5kOyPUrGODevo-9B-t_SXoK3kafRMfk_Ewbr2gfnNpbu6uDb7ZVqm3XNUi9CftUkCfp0_tcTv03mcBh63aF0BzGdljXhOf3jFhkMmOGLqRfqeQ31eJATvtvQKJ4p58ct8PzVuZiYxQYESu_WKPMaL_vpC8pcbQBOG1ah_QiIwWF2yzttY0mciv2DwSgXM8EdHmGHE9YoMAMdceVTN-a1Hecq4bp1iGrOXf5jsM7Yr0xUWmpo64u6LyT0EFzeZEC78vCl6qT6JAz81C2R-vKdgZjzWK6m85h80z3X3-lU1_rvoQzWme9xDnV7gUq154HqxF-0rXZIdLItAoCnImOKNC0-MI3rcCw_Exc=w1600-h900-no)

I also wanted something to facilitate clamping longer awkward pieces which the front vise couldn't accommodate. This is where a tail vise comes into play normally, but I decided to go with something a little different that acts like a tail vise.  Veritas makes an [inset vise](http://www.leevalley.com/US/wood/page.aspx?p=66819&cat=1,41637,41659) which I picked up for $94.  This vise has a very small footprint yet will allow my workbench to clamp items up to almost 6' long very securely.  After drilling bench dog holes in the appropriate spots a bench dog can be placed anywhere along the length of the table, and this vise will push the workpiece against it creating a secure hold.

The installation of the inset vise went very well. With my router, its fence, some patience and measuring I was able to create the perfect void for the vise to sit in.  It's relatively simple to accomplish and I also recommend this vise to those in the market for one.  I drilled dog holes every 6" along the whole bench except where the front vise hardware was. I don't have a picture of me doing this, but you can see the finished dog holes further down.

![](https://lh3.googleusercontent.com/uwMNxjhcAr0DYOfuMiaEeJq4ZuvfEvlVvHjQFdYTav8hOOjgvCMgv29cJYARWx3TxTx7ICguy71vKqxe6ZLmH_SnXWF-1xdjnZUoz5zqwWablY1HZMoECB4KDxXLmD9WYO4lTOLkaewT5cek67Kl-1pahvo1ucROQun1fRGBbPjinJthi0a0PT_OC-r4eV5oIQodAYDJeM9hLDH6oR1_o3bhMcl8Dri45oX7n11Xg8ybRrl2jjlHtO34r08fhRvT2PFr3gmhsALFkwCNYeUZTINxg7lZVye6HgNzZDFcb7-1d96okGfmYwaixAyfSujxv4gzXTQoWGR4KGs9YeWEoyLsdrgpnCD0Fig9YWlE2zTDJEhhXdMJ4cm1M1AnknNKi9Ky1PWnVbYQk97T6CSIQNV2W84jBNTufAaOLBzXiKEi_Gy1Oxb3wJhXP3lb48Pi4V-xpYdkHi2ArfsjhXkRE4KFG1H2qhacBk4pOy_fSae7r_ydzZChoeYpiBYiNTIeZMDx4W-AqHNwyFtbBiD8vt-NEMflta8R5pLrFBUpygk=w1600-h900-no)

![](https://lh3.googleusercontent.com/xadwm67yrhU8T5m_VaJl8m_MceuKJ66KSIhz4noDr_x_g1VCuf1pW9QNbRxqPAOkpjVjO6grZcaYcMgZg0FeyblZyWWCJJFLl-zTY_8XL9zGSi9ZouYCj1NhZ1cRgNptMQV3gq90Mi58AFo-jz_NB8hEH4oAQV6Hewx1onNHKrXSrrPvawRZFh893S_5a-EyXOdp-SXQFOBEAPwkWiFo0Wc0p2TeCTdEcVGBfH8b-9n7eh2hgMyBMhm7CtUa3TEPLNMJ3WLVthp14vndHTYqKYkitG6AkKlZZacZYvDW0NOdNqTZk9jfCCGlcpQS3t6pF-sSj0jFEbpNMNHtPfI3rzHGynzl_GS24SMFzgbM3wTvqilWDweQ-CtjjMWpRDnLW0aKXPfkz22YOrTGfrbJV_zzjoFnQo7fJ2e85JSVOg6sevXmSp6kfTQbJFCsemLhv9pL6T0ZFkq5VSK6Cq7kbuu0xsPG_F2ilTEoFX1TrLu71dA4CKLB3dGBHcASQu7jq8rw2T2DL7Fh5EndlzLgN8JaMnV7RNhOwqCmunrUqeQ=w896-h1592-no)

![](https://lh3.googleusercontent.com/5TRoQEUmkU3iuxOGNrrCdThDpdOzU-_i3dTkT_-PdctwlkgnLfKZgbN51vKOeYnCbj1MwHVf-6o8iOuUaxBW_9tecU2xuzU-SW3iV3n4OhfpJa3gNApmoEIOxLThP55qlUT38I3JtBHLlJ6-8PJYLarcU-PGb2-mCVvIivn1M6msoYzsI3Gf_QxrZLINLLdMHD1e4PakuSKtJuS1FbIPtwNIrC2q6UJdtjlKj5Qv1-aCRrUkRfH-KmIERHYsNL8MCszTtQB8rDmRxG30ICW4UCoIzcQmvJC4M_sYEtOKMSuI3kfzPEFVGQ3PvkoN-k0WzIlwXqkhkhRHMbn0DsXTVfSxNXc2GH7KATKomXHQJZvpklE8bl2HO0IJKylHMIsuVyjnqa02CEejLqlRkInBFrYdfnQ4ciMpB4UxJKo_NCDPOmLtR5fluvVbOyUFOFbKxtBHzmuQL6-bPWs5HrnWUcmDtN-0GcXrjXLhkZt3UghU6Ss4d04vW3TcjhgiIP5b5SIH0mcA9AkatEtK7Bx3KbGYNw6fp6F0E_x5CDYEpdU=w1600-h900-no)

![](https://lh3.googleusercontent.com/Asb53E44Eg88XvQEgd7wB-dM3Eadnydgz4FTOy8ELclS6CgIZP0QDW2R5VfoXkkJ_o-KmS0FD3t5SqixL3bq2SXamAv3yDM1TeyTX9rHoQ-QYUFsLhKGnko89wGBVkzGKCddyduukcOLKNpLlYO8MR2Cupe3eIdch2TX6A5Xpgi9WweTbPUdyaWCig0Hu_8PSzeuZj_SZQennrIlYnHYz-WPhT1-EPwVAB3Ld5UcREhSnjOOfcgcxnsyZ7uLwPrTy6ASuYXkx7tf9eZk7oQyzLEYwmuIkmbE8J3i_2XYlHt2AQ7lOZkqPP40P9ynS0AxMwf7GNAYrVsLtkaRKpccUt4Pp73TgSLv2zGbhGxUdZwx4tZtW16p6GUGYlH_aiuoT8XL7Y79-6X_VzRnjd-_JDvnyCoCh3ch7r2nEelPjYm4QWLX7ll6szN4asl9SsxZ6ODC3sH3fLNvMuicKG5wCGLxO2BgRl9gCGKGk4XRch5Tpq2KHHJBQK5793qOuPMVW96M_uSltC3S-Y6ITsEvIKvGIu-ThszJYfEa0-4lHl0=w1600-h900-no)

![](https://lh3.googleusercontent.com/y24Lk8JRPSu9qn7oLPYYiBRPRHUtqt4wMBPF_2Bes9e36mZKYAFDpqmPyp0PZxan_Lu5iLtm3BNsUcgMEpMAvUJCrTaxsJWe7YdKzdVXS83p8MkjNrPVSEMa-1Gu8oyaP-r-h4BvHR7mKYwHWi8GifO6K1-ptYX0ygyorJ-bfv4lOPQzqmNNgLfXPmYPm8Ui0EVFozOPV9CWjY6r_guhLTurIufJMIS7xkCaXglARu6EJoZ967__bqdxpwA_tQNiVlrVoGeZwB5hLrPX-Bvj1meRLN0sw0WWGXM9ooqLKChNjhFMKvSCNbAWA8VkDXEObt0su-aA3bW3XNrDL9Riaf43sFfKmirW93xfye8LA1_-bXZoIsQu2lr-XzQzbXSewZ4H8-Qqa4srtBo3ghsu7lw14SsuUDoWyjrEzCxmFTkJyzS8l2paQJf-4VRzODBZOtIsbmM6yJvU9Gireg38dpqMwkdNrd8CLX3DjSVvVYbqw3ZAMGhjEjnGNBt3qqPrqlffA8uVgyF-Qbmtm_bd5zdHFlkrPytGsIckml4Ji-o=w896-h1592-no)

The last thing I purchased was a couple bench dogs, since I figured I could start off with a nice manufactured set instead of homemade.

### Finishing

I started by filling a few cracks with some wood filler and leaving that overnight. Then I sanded the whole bench with 60, and finished the top surface with 120 grit. Next I finished it with natural Danish Oil.  I used two coats on the top for that extra bit of hardness.  After applying the Danish Oil and letting it dry, I applied wax to the top and buffed it to a faint shine.  In addition to the shine, the paste wax adds another layer of protection.

![](https://lh3.googleusercontent.com/6qCLJDgDiolFe7bz3cmCg_R-5jTMwPSPSVe2AObFothJU5RPc09NkfZLALfPfmnWsAZxEicoipzYsl-jEtjOwNw8PZNX_X1GZEfZXkHba22avlHsOOz-kCo-XYKHqcgR68xyUMz0YFJPemREOVvgBT64NraDAPc5v0B3-c4TXeRA7Yzc5l-dEljRokKLKI_l9ZUDk8jlJtZ3HNSMMDJFup6lsx8kUCh_JPLX9t8ODEm_5tUh-Phja1vFLFya2GbeZHRkmIRFnz_S3j6g4KVlNNCWPL5VQl3Ay1J5YOLhMFjRM1pVVa6mdjXoBSheTXY7P1umG2_Z4gEASdOvGZhc1zqyZrbbKoOxA70-7BAhzx__ps6X2lceO_WJIoazrA2GiahHiYRiarZo_IlbQd1V_414gMLIPTB0ffVzFIApkTKw_H9PnAb5dOmS37K4UQLNX5mNKZolUgfTPd9RGf7-T6afOaSec6mY7JxRT8YIKR_csdy3u1LLgv-VciVn5K6KG9M1MIlvqqFYZw08y2ltc_IVR4PuxFDBSTuYNYC1WjI=w896-h1592-no)

![](https://lh3.googleusercontent.com/Rwzc9FTSQ7-2p0_5YQJOE_mrlb32Ia_6ORMUTqhXjNFI_1idUPbXuSLXNBgLLJIOY8tlM0XkK2AKGtuZn5Hepgg_1PFsJvphrB91rEYd9KXOpcnQEFin8f8jHXPeXYMqRf9jmP_hNeC-WqIc6z39_0rvygRHuXDfVgWX0bLel_gWarGLPL777XW2jVo4WhCBCx9NLRCwyWTw7CfKdbuUl2-BWZJKI3KWwHuFBePboYgc1t9lmt33xL2kXEjlKh8-Egd3PXgwZZmg2aQ5wnecwC6R8IzMqXEMUpGLbYpDFpvGLIxhLRPo9fTQGwfWUESxAmoAt7lEKcMvXLjegf2_yrLURbrifNCbWmnhiQ3lkDuOGftMIGXG54y2WnE_L4mObBToHnmNv4Am6k2qHiS8zdr6CgPFAByWf2Y0FJ6wfhowTI7IReC37yW04yJOsm3j5fgcradJLL-uHYW0vL808wKEwu6uFowtA1VdX_2KepzCKMduGI1bEzfmcEIBEetj15Ct-YlR4w97RQ8YxvqI24EirWsH_2dc8sH36zvuAdA=w896-h1592-no)

![](https://lh3.googleusercontent.com/bxf7-fRIBJMikOF3PSfS_fP0s-Pu4QRg5k5qHZpNHj_4aLz_nAfhZRQpWL9bl31R3aUhwU-cgMsCNU7DL9MhloSv0Qu5Ixelzro11r2_aITKsq-NMoxPNyQWA7DhQlhwn0sAiD8oIaliClB0TQcAW3P66mY-w6-0YxnoC6jNN3W27NAmLeJY1W_DcXjQz4hsArzYbcHK3AOqO2dDHCw16MCg5jSqGk1szWmtTkopBZBBki1gcJOZ5tJ2Fih37MQOmqdOjzo-XQYWKmuYeRboCXHR77AuLh2w__Tvt2vxv6Ty4WZH1-aNOf7zWgRFb_aGWfQCmb04UG0_B8pn3psqDanglpLEmVthkqrAnRVJwVfD7_cgd9pk95stgsF63-eUUVv6SGziCEo1qxPQLcj76hjFFWGJT9mg-VyClXa6h0i1Jt_eIGjC8vIJ7Ae_ilkWApaKzj3pAn5f-fHFb62FjyhfeWvJaaWALmFvx2oYw0D4UDDgh-vcp8zTlE3JHjsl4-357SkgWY2QGPGl8w8jPTT0BXM_PwjJJc-XEg26UFs=w1600-h900-no)

![](https://lh3.googleusercontent.com/i9kaeblqamQWFSiA4LgSCrX3zD-oxFUad4xnmk1Z1-6dAzTcXAt9jdQrGoWFYVb8io4Sp75jvhordUmLzdXhRN-DET0ly2K_BdjAjY1WhUC6Aw5WtNhOdP0PsE9HmIKjAeBfCBs9BYrhjBQwuR8_SkeL129FoW-N5uHATwbbmVX5bOa4LLkh-LbZyQHDDy6RdCDyqW09zPJVe_Cz7ETaKPVH_8tkB-YoXcTZfMVIW8La7zqKQDGqYYDas67QWCsY-MSrz0D9XyACGT1kiEzLo2hPEC8cNPDWu7We2GWT-hKdZyoEVvyMXR7ddIp0seTsfXZfrB6gODxnFEBXl0_9cI9jAPJ9Oi6TcJE9JT85g4sRdquoOzNwA63NysLmgV9sOecdchGiTLN92qsGFd8pStZg9CyREjlblhqjhzau6o9stAsQZDSVsoknBiauAfjOnr0MiN35hjohIQ3z0ysUi3oS-ia-QRr4Lmte3CYjAkl9-hiYv_3UHmD4Y5tyDs7cG7-Ujiv2jkE60exbTmcFgnHN2vdo9pMzlE5G0Cc7V24=w1600-h900-no)

![](https://lh3.googleusercontent.com/0QU1FmkOOmBc1zrbX4xixc0_AKDRNqBZzSVLC6CPHxCYOBWM6nsMFGRFZFJP1b3Eynw1xXqtpmeaCGIWZvWhRkmyA8ymw_4245_BPcfiZQ-e583NsUR3735aaqhRB7igBdQlq0upEAcBwdcsd-VvdT3SJ7kQTxq8GGzWE1TgFrxvAsXymu5fm_ON6FzzWE47CMxlsQhGtaXGXfWx7vX_2drhY9I1wZqmhrvGIOVzMqu4hCGWZEZWUd36lSvl-qFk6L1qulWXs_LkusdZEhpJIXQeRFTG_pamAf1uClLSQBAcBfB-W7ljgGxmNW1e6UvoztQwo1zY-HJZ-Pmqz_axY13p_3Z2nMd_QgbkgG1VUTGRcIovd5Z8EYMbGtxVRF1KBbsMH6uKuh0Q_Dh18_BA4dH7Qj7b-8hP6zSCqDcxhr_1Oupr4PLm-mkptR9unOI90lzTTZse_pno-pF0iF_DyzURpWm20Hsc0rellEYPuusD13JVAgmbuVnD-4b-WJpV2oUBiZAc7NV2fy0-M1RcP1FDFWxAZrosEQVraBdbIIU=w896-h1592-no)

The last thing I did was put slats between the crossbars on the base for some storage area.  Here are the slats ready to be screwed home. I used a 1/4" spacer to get even spacing between each.  The first slat was put in the middle and I worked out from there. I also finished these with the same Danish Oil.  Check completed pictures at the bottom!

![](https://lh3.googleusercontent.com/QXqXjETcSN4F5fQlPmxQtf9iZJK4YEHy1NLAY4USUOROe22nm_LXaRSlcyG1Z8S8JVMcaP3P8kIDmgxQBCKQ2GKxO0oIfE0u284OGADeg6ebuxitCzfdayy1rly-K2_UBjaEKH6wzcpu-4NhXSWofK0id081eJukTNIgMbuAYwKIbj3oM8Wgq68ZIEPK1i3T5OkeVelz2yv2FMizDECof_jhoW5unaaNu3cmp6_9GR_71hUBwf7sY21JTWMdj6d1IyfLNGp3qEYWqbEH603SMznVcV4P8SR-WWdXtK4or83aV9fan2CK0BoHoG-NUPBrvrZ0zLVV7XJAXtZXYGQltMC6QymlWSW8j2Eb4BrhSKzxN03b4Sw736CDhNqAg3QPzMPtpUe9G9QElTt4WiAD-nbXUcAKzy2Ee4q76Rb_qFc_EjDis-v1eOhEkswaY5ZztqezaoSOLr3M_JmBiG_NS43ISlTtj4RUg2dOokjts9mxTflvP3cyVdRo62D0pSlt9UmCvTmMXeAPjGJkLaytKOGOpWu-stEzJz5oJc2YXxE=w1600-h900-no)

### Conclusion

I believe I've accomplished building a very functional workbench that will serve me (and maybe others) for several years.  Adding up all the costs:

- Vise 1: $135
- Vise 2: $94
- Finish: $15
- Wood: $65 (with dowels)
- Bench Dogs: $20
- **Total: $329**

This is more than I estimated this project would cost, but the bulk of the it comes from the hardware.  This could be expected, but I'm used to hardware being a couple hinges and screws or nails for my projects.  For $329, however, I'm able to have a solid working foundation, and easy clamping capabilities.

Thanks for reading!  Here are the finished pictures (with a clean shop).

![](https://lh3.googleusercontent.com/f5d-l4qzbpNlJblJytrN8ai15Pg0pCZrLW4Wi6Xndlj8Y4lH-zR-DLfh91X-4YjBSRVlCEaESIndQWBDL9cyPxjJKopWnjz4NUK5T6L5d_2qanFvpKPtf7_ZE40ijdWk_JB85phrQrS15jioYAQg1T-r1btLVWkZ0huckTStISLsp_x3rVqMwHEXySzyOLke0_N7mHCE1VPBLwkDEJXypoJTgJ8yvwSrMvljZkcH3VCesd2VlVukvGFovCXiUOUEHxiAof0RbA0xvYkoWdrZEq6Fd_MpA4pk2MVSxIYOj6mL-LRS3ocsNDbXmYED3N83xEHOakWcV1Fy5kV_7t5EeP73NX96Y6rOZREk_qUP6fjZ2H5QfnaBdZHhjy1fZMNqisZdPL8QMD_mpcJbPe4L0DhIlmaKWxxaF_v-BTOBW2g9wBWa-XtgBq85U8h4LO68F3Z7rkSKstVkDlNFDH_gheIyvsRiLYTiNnzPc9h_urmFyfyD2jtVsTZK3XUB9yVe3eOFUooG21TFcrMO9Rp935r0FJBg14jHgIxoafY5K_I=w1600-h900-no)

![](https://lh3.googleusercontent.com/MMylogsOoTvmgxV826xbAn7IHJMrtBmNgod8pbev-ON8kgXMnbngv9EHyeyFNYgcw7dAvmjYSpH3X8oQa8RirAF4v5IYy1GiKuhAzYCbEMmFaZ-aU0966pu34FAPK9sndjNXpWQ-DOoqt2gOezhS0cy0zA_-Pd7vM7MgH07xNUv3jX3k9haN9-jTx5s7TyUdQcBO2H5GBxK6JiHEd7bVbAQfKhFN234cdZsFZhf8yHmzAJtwvR3D6n0iyughtc_Ne45ccFS14NEnEObBOaKdwxbcG5He0g8lLeJIrLAudTTxztvWQiPllFMrbuK6X7lMb1jxhBXdv_DAiDq352fXL1uaZcZAiUB5utiqMxW4TrJOuHfhwcrriQwd370GCw_zW4IDb6ymi5nIZWaajhbzorCenDrdngfznlY-u7fG5j-_zdGf09v8hqYExhv_03IHmYSBIVEODJ-Tq7tjXdPtm-UqgGI0BEfLBvfvfmyDEnuXraafgIdeAjC90I8obmDOE-9uUQn8I1R95EX8Rc5ct_Xk0azyYGT-j8Vsgcp-b_M=w1600-h900-no)

![](https://lh3.googleusercontent.com/II-TQyxk-A80WqJpiVjOWPfIWcbsrcuxXKPajLFcsyc1yPoI6XBYnUw8O9oYxPbaOXQarke_Ey56EQnOnzCsjAVg9wuyOmQ6c840jBrlL9jfO_X_B-Tg3hNQjtDUHoTVGNjt2G_N1gmFim6Wu_G-qauog5H3p-VshefbHxLUT_8JTBIykK6U0QIXXKrLyLE3ki60wg4CEXgrW9LCbIlz3ur6wdGO9dqnbYHnZLoWtNtPtVSpXdFPylOwLWUckNKd9b8e7aq_uFeSSJ1OEYVdxwAvjCinD1XPZ7MNv5WeUY71Z_htlREKM_J2ZXdK1pVcEpvSFNcBdryA0ZMymXH7_QpIkPUNc42WymtjU6kjTKaDpYr1_Dic7mS-d9eZ7Oc3MCfL-2iBXrtLjWqQm-KcWBqinBaoo_bWRoKBJZ__t8teR6ksPRJsY0wspKTLHRmaQ4AAeh8YUo_9iXGTg7_JvjVCmWwYraEg0kpTgkmf4I1sVlq1VT-NZJpxaXZkDcvwTGPQtEnE99dgSaCL5YqttquBYl6Fhj10BvKCwQg6V_o=w1600-h900-no)

![](https://lh3.googleusercontent.com/bte47UO9fHTq6pwiqucexVmVYGuYA80ykQkqtJ8u8rUy0WGPHd2nLMBCj7Wl0xiGlhTXYWUkUiGmtfJ7Q4zlDiQ99yry1Jo7HV6A_6qwJlIp3psdwTsOuhskUAZPEaUqX_4jJCvlnwTZ44MlHv7Ho79HL2dxm36uD6k4-8OAnN1HxxX4x7Sd60H2lUGKEp1pCN0k3CJQVYSBiX3M_mPbAdIitqDTCfPzgsq-aO4nwqBbPTIl_9frBmRts2MXuNS-D2njlOlrxiDim1UhI2hpdevNJO6jRGHj2W3jZMPNDrU7fl7jhC_yyUdqLtG5TLeJkcIyc6pnQy-LSgtK-piMuunyuHiIbPiOqqY8xXyy4uPTHMqdzKNelFsxsV7_5sZPZe4ja_gqysRgTrDYFpPZ5IMuJkNlV2dOUN300l_jtDn18O7CHr5Kkh9juiP53y_Am1bzmCp6QzSDbg5WnFaMhZDM7u38lfQ6KpgxXghg1vt_qnQ4wh_Co_RgZPnl6Gcm5qcvCnLsjXozEU1E_YcO2vCTzvE2dTBQK5PTIDIp0DU=w1600-h900-no)

![](https://lh3.googleusercontent.com/rt3uZMBiu9-d2OPgyVUkr75bsq3ojJrlLqieI3M6tYUG69FYB7QvG7iOQleSuyh4sx6MyAZVyBKOul2EVTAn6axWmMsZNwoJfzwmbm2PxkqzLM7nhBUTIHARv7Kom2jrDBCv2aC8TQbyVGdcHty2xYplSZHO9IMqVbEr1jLy8OGsKjrDLUnMSk_LkxwL62IyV75qFCivW28X8WZsGaZ1SFUBlk-G4Ixr3q6-UhBtKx7Dp5PkXCD1YH2ekaI9AHn2XzFt3kxzHy_3Pl5d9JTF3Wir7irr-r7vUAJwZYRGkhnveM_A3FY_Svoo5sroMlfOGba8QCg6RSBPyvovRQafNLFWyzCkF9AKAmnh4F0CgckBJwersy4HoBSm53D7ymIe--0Tw_B3aB4pO7l56230bZbberqv7-pft_yA6T8Wx0GfM4IklYGjB02txMb7qXzLCuaITDRGiCJdxM9EGMg0jIo0k0ncejcJiDmw2o6OFeAc3x1KDoe6UP8Le2OLUUO3xl_uNTV0CSC9fQoAYzKUF0_BzpSarX7qsJ9e9-7h5p4=w1600-h900-no)
