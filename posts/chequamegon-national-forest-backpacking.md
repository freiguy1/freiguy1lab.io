layout: post.liquid

title: "Chequamegon National Forest Backpacking"
published_date: 2013-08-09 16:29:39 -0500
---

My friend Eric Johnson and I went on a short backpacking trip through the Chequmagon Nicolet National Forest Medford section this last weekend, and had a pretty good time. I took along a notebook and pen to write down some notes and also took my old Kodak camera to take some pictures. This post will try to merge the two and hopefully lead to an interesting read.


## Day 1

So it all begins. I'm out here next to Jerry lake in the middle of the Medford district of the Chequamegon Nicolet National Forest. This fairly large piece of national forest land can be found in the north central region of Wisconsin, my motherland. I came in my truck and parked about 1.5 miles southeast in a small parking lot. I arrived at about 6:40 p.m. because I worked until about 5. After eating a sandwich and loading up on water, Bo and I headed out with our packs.

![Bo with his pack](https://lh4.googleusercontent.com/-etfBeUxQGJg/Uf6ylEtRVwI/AAAAAAAAG40/EfCyuuWU1SY/w702-h527-no/100_2813.JPG)

Right away the terrain was filled with small hills and old-looking vegetation. The trail reminded me of a very well used deer trail, and it was clearly marked with yellow reflectors. About halfway to my stopping point I reached the wetland south of Jerry Lake. Here I snapped a few pictures and attempted to give my panting beagle pack Bo a drink. He'd have none!

![Wetlands by Jerry Lake](https://lh5.googleusercontent.com/-u75oVQvIlQ8/Uf6yqM5-c_I/AAAAAAAAG5Y/16fEoFkJSpU/w702-h527-no/100_2819.JPG)

Another 15 minutes of Bo leading me and we came upon the first primitive campsite I've seen! This caught me completely off guard as my map did not have them on it. Well that and my inexperience probably helped make it so much better. I had not been there 15 minutes and I hear a female's voice, "Hello?"

![Panoramic of Jerry Lake](https://lh6.googleusercontent.com/-04IJXVkqtLg/Uf6yxK2wwzI/AAAAAAAAHEQ/5mBCtkzXQ1g/w702-h180-no/100_2825.JPG)

Out steps this younger woman probably about my age. Her name was Rosie and she was camping next to me with about 5 other girls who were in a wilderness therapy group. Rosie was their leader and she wanted to make me aware that they were there. She found Bo very cute and asked if it'd be ok to come up and introduce ourselves to the group. I readily accepted and went about 30 yards uphill to where they were all camping. It looked like a lot of fun and they all really enjoyed Bo.

![Hot Chocolate!](https://lh5.googleusercontent.com/-HPKO0mI7QCc/Uf6y_GxGPgI/AAAAAAAAG7I/AaNCaD6t_HU/w428-h571-no/100_2832.JPG)

After saying goodbye, I made some hot chocolate, hung my food in a tree, wrote this and now at about 9:21 I'm going to retire!

## Day 2

I woke up at about 6:30 after sleeping through a more or less restless night. For some reason I couldn't get myself to sleep for more than 1-2 hours at a time. Part of the cause may've been the fact that I forgot my cell phone (alarm clock) in the truck. I was using my camera as my time-keeping device and that meant I needed to check it often to make sure I wasn't sleeping too late. After all, I was to meet Eric at 8:00 by the truck which was about 40 minutes away.

![Jerry Lake in the morning](https://lh6.googleusercontent.com/-0MCj4bkZrHw/Uf6zBILDEpI/AAAAAAAAG7Y/CEwBvqCWiFo/w702-h527-no/100_2834.JPG)

I got to our meeting point after eating some oatmeal and hot chocolate at around 8:10. Not bad timing. Eric was there and ready. We drove my truck down to Highway 64 and started on the journey at about 8:40.

![First water we came upon](https://lh3.googleusercontent.com/-mxIbiEdRRXM/Uf6zOvUPRJI/AAAAAAAAG8w/Hne_4RYuQbk/w891-h668-no/100_2843.JPG)

![Eric's loving it](https://lh3.googleusercontent.com/-pPrDntar9-I/Uf6zYayq2DI/AAAAAAAAG9o/SQU4VcIL-5A/w501-h668-no/100_2849.JPG)

![Water](https://lh3.googleusercontent.com/-LJ_HPiI8D4M/Uf6ziWde8YI/AAAAAAAAG-A/CY7-gcVwv_I/w501-h668-no/100_2853.JPG)

It was just the normal woods hiking most of the day. The temp didn't get hotter than 75, but we were in the shade so it was even nicer. There were many raspberries along the way and many were perfectly ripe. Bo stayed a good sport and was able to carry his 5-ish pound pack without much trouble. He is now completely spent and has been sleeping since we stopped (3 p.m.).  The first primitive campsite we found was right before the first road crossing. It was pretty nice with amenities like flat ground and a fire pit. The second one we ran into was next to a creek but I can't remember where on the map.

At about 2:45 p.m. we ran into another group of girls in the wilderness therapy program. The leaders made all the girls stand with their backs toward us.  I'm not sure why; it was quite strange. A little past them we came upon our stopping point for the day.


![Stopping point](https://lh3.googleusercontent.com/-Ktu7fDoFnaA/Uf6zwidTHpI/AAAAAAAAHAg/M9iGpee-CJU/w891-h668-no/100_2868.JPG)

We pitched tents, sanitized some water, made food, cleaned up, hung food, started a fire and now at 8:00 we're completely exhausted and ready for bed. I will put out the fire now and continue my quest for a good night's sleep.


## Day 3

Sunday Eric woke up about an hour earlier than I wanted to. The low temperature [was 44](http://www.wunderground.com/history/airport/KMDZ/2013/8/4/DailyHistory.html?req_city=NA&req_state=NA&req_statename=NA) for that day in Medford, so it wasn't too bad to sleep in; it was maybe slightly cold for my 20 degree bag (20 degrees is a survivability not comfort rating). I was so tired from the day before though, that 11 hours of sleep was pretty easy to do. I rolled out of bed around 7 a.m. I think. From there it was getting breakfast rolling and packing up for the trail again. We knew that almost the same distance lied ahead of us as what we covered the day before. Judging on how our bodies were feeling last night, we figured this was going to be a long day.

For breakfast, I had prepared a cornbread-type cake thing. At home I'd mixed some store bought cornbread with some cherry craisins and almond slivers. Then when making it, I just needed to add some water and *try* to cook it slowly. With a MSR Whisperlite Universal, that's easier said than done. It takes a pretty good touch to figure out how to make the burner not extremely hot. Eric had some peaches 'n cream oatmeal (4 packets actually). We packed up the tents, took care of the fire, cleaned our cookware, packed our bags and took off. We were on the trail by about 8:40.

![Lake Eleven (yup, that�s the name)](https://lh4.googleusercontent.com/-4Blz4Pue7cc/Uf6z110C6UI/AAAAAAAAHEI/D3BbdNSIAsw/w1263-h346-no/100_2875.JPG)

![Raspberries!](https://lh4.googleusercontent.com/-yMnBn0Yp168/Uf6z3uyyYTI/AAAAAAAAHCM/UjdKTYo_dls/w501-h668-no/100_2880.JPG)

The rest of Sunday went by very quickly. We never saw any more people and for some reason we were like men on a mission. We passed a couple bigger lakes than the unnamed (as far as we know) pond we stayed at. Much of the trail today consisted of walking atop a ridge for a few miles. That was pretty neat as we could see down both sides. It also seemed like we were changing types of woods (not sure of a good word for that) every once in a while. We'd come down to a valley and there'll be only maple trees as far as the eye can see. Then a half hour later we were surrounded by pine trees of some sort. At one spot the large, old pine trees were so dense that there was no undergrowth just pine needles, yet you could walk through without a problem. This seemed to Eric and I to be an optimal camping site.

Around 1 p.m. we came out to where the White Rhino sat (Eric's white Dodge Ram). Bo wasn't aware of it and eagerly crossed the road to continue on the trail. After calling him back and putting everything in the back of the truck, we headed to where we began so I could be dropped off at my truck. The ride back to my truck was 18 minutes. 18! Hiking approximately the distance it takes a car to travel 18 minutes in two days isn't too bad right? Well I'm telling myself that. After this short ride my legs, back, and shoulders were starting to feel it. The change from moving for the whole morning to sitting in a truck was a surprise for them. Shortly after arriving, we agreed our first backpacking trip was a success and parted ways.

![Blurry picture of Bo waiting for us near the end.](https://lh5.googleusercontent.com/-NaZcrOdPCGw/Uf6z5-CYcGI/AAAAAAAAHCs/TqnjjUq1NJo/w891-h668-no/100_2884.JPG)
