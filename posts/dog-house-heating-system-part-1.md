layout: post.liquid

title: "Dog House Heating System - Part 1"
published_date: 2013-10-16 09:55:13 -0500
---

In case you're interested, here's [Part 2](/posts/dog-house-heating-system-part-2.html). It's time for another project! This time I'm going to be installing an active heating system in my dog's house because there are many times where the passive system (straw bedding) doesn't quite do it for him when I'm off at work during the middle of a Wisconsin winter.  I'm going to be using an infrared heat lamp as my heating source and an ATTiny44 which will act as a thermostat to turn on and off the lamp depending on temperature.  This should maintain the temp pretty well on those cold days.

Here are a list of the important components:

- Microcontroller ATTiny44a ([digikey](http://www.digikey.com/product-search/en?pv16=6510&k=attiny44a&mnonly=0&newproducts=0&ColumnSort=0&page=1&quantity=0&ptm=0&fid=0&pageSize=25)) ([datasheet](http://www.atmel.com/Images/doc8183.pdf))
- Infrared Heat Lamp ([amazon](http://www.amazon.com/WATTS-HOURS-LIGHT-INDUSTRIAL-GRAD/dp/B000STDLFE/ref=sr_1_2?s=hpc&ie=UTF8&qid=1381862805&sr=1-2&keywords=infrared+heat+bulb))
- Relay ([sparkfun](https://www.sparkfun.com/products/10924)) ([datasheet](http://dlnmh9ip6v2uc.cloudfront.net/datasheets/Components/General/Relay.JQX-15F.pdf))
- Temperature Sensor tmp36 ([sparkfun](https://www.sparkfun.com/products/10988)) ([datasheet](http://dlnmh9ip6v2uc.cloudfront.net/datasheets/Sensors/Temp/TMP35_36_37.pdf))

![Prototyping](https://lh4.googleusercontent.com/-PlQKCdaxeIQ/UmGZBAEoFTI/AAAAAAAAHJg/sEEZAZ7I-ME/w759-h569-no/20131016_193128.jpg)
Here is a video of the prototyping stage.

<iframe allowfullscreen="" frameborder="0" height="356" src="http://www.youtube.com/embed/1dyv8QAL7Kg?feature=oembed" width="474"></iframe>


