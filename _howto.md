## Adding a post:

1. Open 2 terminals, one for text editing, one for running dev server
2. In the server terminal, run `cobalt serve --drafts`
3. In the editor terminal, add a new post with `cobalt new -f posts "Name of post"`
4. Edit the post until you're satisfied, viewing `http://localhost:3000` in the browser
5. When ready to publish, run `cobalt publish posts/name-of-post.md`
6. `git push` your changes.

