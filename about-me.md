layout: default.liquid

title: "About Me"
published_date: 2012-05-08 10:18:52 -0500
---

### Ethan Frei - n. (EE-then fri): Software Engineer

<img src="/img/us.jpg" style="max-width: 100%;" >

I live in Wisconsin with my family and our dog. I build .NET core and React applications for a genetics company.

## Technical Interests

I enjoy exploring new programming languages and frameworks. At work I use more mainstream tech, but at home I like testing out newer, more esoteric tech.  Over the last few years I've been particularly interested in [Rust](http://rust-lang.org) and [Zig](https://ziglang.org) as systems languages and I enjoy using [Elm](https://elm-lang.org) as a front end language.

I also enjoy writing embedded software and building embedded and IoT systems. I've worked with [Arduino](https://www.arduino.cc/)/[Atmel/Microchip](https://www.microchip.com/en-us/products/microcontrollers-and-microprocessors) products, [STM32 ARM microcontrollers](https://www.st.com/en/microcontrollers-microprocessors/stm32-32-bit-arm-cortex-mcus.html), [Particle](https://www.particle.io/) boards, and others.

## Other Hobbies

As of late 2015, I got into woodworking as a hobby in a big way.  My wife and I bought a house and it included heated/insulated space in a detached garage for a [workshop](/posts/new-house-new-workshop.html).  I purchased a [table saw](/posts/sled-desk-and-table-saw-outfeed-table.html) and have slowly been building the shop around it.

I also love the outdoors and everything that goes with it: hunting, fishing, camping, hiking. I was born and raised on a dairy farm in Wisconsin where the outdoors was life.

